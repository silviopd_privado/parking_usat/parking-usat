-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-03-27 21:23:32.15

-- tables
-- Table: cargo
CREATE TABLE cargo (
    id_cargo int  NOT NULL,
    nombre varchar(100)  NULL,
    CONSTRAINT cargo_pk PRIMARY KEY (id_cargo)
);

-- Table: correlativo
CREATE TABLE correlativo (
    tabla varchar(100)  NOT NULL,
    numero int  NOT NULL,
    CONSTRAINT correlativo_pk PRIMARY KEY (tabla)
);

-- Table: fotos
CREATE TABLE fotos (
    vehiculo_dni char(8)  NOT NULL,
    vehiculo_nro_placa char(6)  NOT NULL,
    nro int  NOT NULL,
    descripcion text  NOT NULL,
    CONSTRAINT fotos_pk PRIMARY KEY (vehiculo_dni,vehiculo_nro_placa,nro)
);

-- Table: incidencias
CREATE TABLE incidencias (
    id_incidencias int  NOT NULL,
    descripcion text  NOT NULL,
    dni char(8)  NOT NULL,
    nro_placa char(6)  NOT NULL,
    CONSTRAINT incidencias_pk PRIMARY KEY (id_incidencias)
);

-- Table: lugar_cargo
CREATE TABLE lugar_cargo (
    id_lugar_cargo int  NOT NULL,
    nombre varchar(100)  NULL,
    id_cargo int  NOT NULL,
    CONSTRAINT lugar_cargo_pk PRIMARY KEY (id_lugar_cargo)
);

-- Table: marca
CREATE TABLE marca (
    id_marca int  NOT NULL,
    nombre varchar(100)  NULL,
    id_tipo_vehiculo int  NOT NULL,
    CONSTRAINT marca_pk PRIMARY KEY (id_marca,id_tipo_vehiculo)
);

-- Table: modelo
CREATE TABLE modelo (
    id_modelo int  NOT NULL,
    nombre varchar(100)  NULL,
    id_marca int  NOT NULL,
    id_tipo_vehiculo int  NOT NULL,
    CONSTRAINT modelo_pk PRIMARY KEY (id_modelo,id_marca,id_tipo_vehiculo)
);

-- Table: parqueo
CREATE TABLE parqueo (
    bloque char(1)  NOT NULL,
    numero int  NOT NULL,
    estado char(1)  NULL,
    CONSTRAINT parqueo_pk PRIMARY KEY (bloque,numero)
);

-- Table: persona
CREATE TABLE persona (
    dni char(8)  NOT NULL,
    codigo_universitario char(10)  NOT NULL,
    apellido_paterno varchar(100)  NOT NULL,
    apellido_materno varchar(100)  NOT NULL,
    nombres varchar(100)  NOT NULL,
    telefono varchar(150)  NOT NULL,
    correo varchar(100)  NOT NULL,
    cargo_id_cargo int  NOT NULL,
    estado char(1)  NULL,
    CONSTRAINT persona_pk PRIMARY KEY (dni)
);

-- Table: registro
CREATE TABLE registro (
    id_registro char(9)  NOT NULL,
    id_lugar_cargo int  NOT NULL,
    motivo text  NOT NULL,
    fecha_entrada date  NULL,
    hora_entrada time  NULL,
    fecha_salida date  NULL,
    hora_salida time  NULL,
    vehiculo_dni char(8)  NOT NULL,
    nro_placa char(6)  NOT NULL,
    usuario_dni char(8)  NOT NULL,
    bloque char(1)  NOT NULL,
    numero int  NOT NULL,
    estado char(1)  NULL,
    CONSTRAINT registro_pk PRIMARY KEY (id_registro)
);

-- Table: registro_detalle
CREATE TABLE registro_detalle (
    id_registro char(9)  NOT NULL,
    nro int  NOT NULL,
    dni char(8)  NOT NULL,
    estado char(1)  NOT NULL,
    CONSTRAINT registro_detalle_pk PRIMARY KEY (id_registro,nro)
);

-- Table: tipo_vehiculo
CREATE TABLE tipo_vehiculo (
    id_tipo_vehiculo int  NOT NULL,
    nombre varchar(100)  NULL,
    CONSTRAINT tipo_vehiculo_pk PRIMARY KEY (id_tipo_vehiculo)
);

-- Table: usuario
CREATE TABLE usuario (
    dni char(8)  NOT NULL,
    clave char(32)  NULL,
    estado char(1)  NULL,
    CONSTRAINT usuario_pk PRIMARY KEY (dni)
);

-- Table: vehiculo
CREATE TABLE vehiculo (
    dni char(8)  NOT NULL,
    nro_placa char(6)  NOT NULL,
    color int  NOT NULL,
    estado char(1)  NOT NULL,
    id_modelo int  NOT NULL,
    id_marca int  NOT NULL,
    id_tipo_vehiculo int  NOT NULL,
    CONSTRAINT vehiculo_pk PRIMARY KEY (dni,nro_placa)
);

-- foreign keys
-- Reference: Table_14_vehiculo (table: fotos)
ALTER TABLE fotos ADD CONSTRAINT Table_14_vehiculo
    FOREIGN KEY (vehiculo_dni, vehiculo_nro_placa)
    REFERENCES vehiculo (dni, nro_placa)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: incidencias_vehiculo (table: incidencias)
ALTER TABLE incidencias ADD CONSTRAINT incidencias_vehiculo
    FOREIGN KEY (dni, nro_placa)
    REFERENCES vehiculo (dni, nro_placa)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: lugar_cargo_cargo (table: lugar_cargo)
ALTER TABLE lugar_cargo ADD CONSTRAINT lugar_cargo_cargo
    FOREIGN KEY (id_cargo)
    REFERENCES cargo (id_cargo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: marca_tipo_vehiculo (table: marca)
ALTER TABLE marca ADD CONSTRAINT marca_tipo_vehiculo
    FOREIGN KEY (id_tipo_vehiculo)
    REFERENCES tipo_vehiculo (id_tipo_vehiculo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: modelo_marca (table: modelo)
ALTER TABLE modelo ADD CONSTRAINT modelo_marca
    FOREIGN KEY (id_marca, id_tipo_vehiculo)
    REFERENCES marca (id_marca, id_tipo_vehiculo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: persona_cargo (table: persona)
ALTER TABLE persona ADD CONSTRAINT persona_cargo
    FOREIGN KEY (cargo_id_cargo)
    REFERENCES cargo (id_cargo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: registro_detalle_persona (table: registro_detalle)
ALTER TABLE registro_detalle ADD CONSTRAINT registro_detalle_persona
    FOREIGN KEY (dni)
    REFERENCES persona (dni)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: registro_detalle_registro (table: registro_detalle)
ALTER TABLE registro_detalle ADD CONSTRAINT registro_detalle_registro
    FOREIGN KEY (id_registro)
    REFERENCES registro (id_registro)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: registro_lugar_cargo (table: registro)
ALTER TABLE registro ADD CONSTRAINT registro_lugar_cargo
    FOREIGN KEY (id_lugar_cargo)
    REFERENCES lugar_cargo (id_lugar_cargo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: registro_parqueo (table: registro)
ALTER TABLE registro ADD CONSTRAINT registro_parqueo
    FOREIGN KEY (bloque, numero)
    REFERENCES parqueo (bloque, numero)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: registro_usuario (table: registro)
ALTER TABLE registro ADD CONSTRAINT registro_usuario
    FOREIGN KEY (usuario_dni)
    REFERENCES usuario (dni)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: registro_vehiculo (table: registro)
ALTER TABLE registro ADD CONSTRAINT registro_vehiculo
    FOREIGN KEY (vehiculo_dni, nro_placa)
    REFERENCES vehiculo (dni, nro_placa)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: vehiculo_modelo (table: vehiculo)
ALTER TABLE vehiculo ADD CONSTRAINT vehiculo_modelo
    FOREIGN KEY (id_modelo, id_marca, id_tipo_vehiculo)
    REFERENCES modelo (id_modelo, id_marca, id_tipo_vehiculo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: vehiculo_persona (table: vehiculo)
ALTER TABLE vehiculo ADD CONSTRAINT vehiculo_persona
    FOREIGN KEY (dni)
    REFERENCES persona (dni)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

