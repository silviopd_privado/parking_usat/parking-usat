﻿-- DROP FUNCTION public.f_agregar_mayorista(character, character varying, character varying, character varying, character varying, character varying, character, character, numeric);

select * from f_agregar_persona_vehiculo('46705011','141TE51191','PEÑA','DIAZ','SILVIO','978438896','SILVIOPD01@GMAIL.COM',1,'ABC123',1,1,1,'ROJO');
            
CREATE OR REPLACE FUNCTION public.f_agregar_persona_vehiculo(
    IN p_dni character,
    IN p_codigo_universitario character,
    IN p_apellido_paterno character varying,
    IN p_apellido_materno character varying,
    IN p_nombres character varying,
    IN p_telefono character varying,
    IN p_correo character varying,
    IN p_id_cargo integer,
    IN p_nro_placa character,
    IN p_id_tipo_vehiculo integer,
    IN p_id_marca integer,
    IN p_id_modelo integer,
    IN p_color character varying)
  RETURNS TABLE(r_dni character varying, r_nro_placa character varying) AS
$BODY$
	DECLARE

		v_dni character varying;
		v_nro_placa character varying;
		
	begin	

		begin

			SELECT dni INTO v_dni FROM persona where dni = p_dni;
			if v_dni is not null then 
				RAISE EXCEPTION 'El dni (%) ya se encuentra registrado', p_dni;
			end if;

			SELECT nro_placa into v_nro_placa from vehiculo where dni = p_dni and nro_placa = p_nro_placa;
			if v_nro_placa is not null then 
				RAISE EXCEPTION 'El número de placa (%) con dni (%) ya se encuentra registrado', p_nro_placa,p_dno;
			end if;

			INSERT INTO public.persona(dni,
						   codigo_universitario,
						   apellido_paterno, 
						   apellido_materno, 
						   nombres, 
						   telefono, 
						   correo, 
						   id_cargo)
					   VALUES (p_dni,
						   p_codigo_universitario,
						   p_apellido_paterno,
						   p_apellido_materno,
						   p_nombres,
						   p_telefono,
						   p_correo,
						   p_id_cargo);
						   
			INSERT INTO public.vehiculo(dni, 
						    nro_placa, 
						    color, 
						    id_modelo, 
						    id_marca, 
						    id_tipo_vehiculo)
				            VALUES (p_dni,
						    p_nro_placa,
						    p_color,
						    p_id_modelo,
						    p_id_marca,
						    p_id_tipo_vehiculo);
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_dni::character varying,p_nro_placa::character varying;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE