﻿select * from f_buscar_persona('46705014')

CREATE OR REPLACE FUNCTION public.f_buscar_persona(
    IN p_dni varchar)
  RETURNS TABLE(resultado integer) AS
$BODY$

declare
	v_resultado integer;
	v_dni varchar;
begin
	begin

		select dni into v_dni from persona where dni=p_dni;
		if v_dni is null then
			v_resultado=1;
		else
			v_resultado=0;
		end if;
		
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

return query select v_resultado;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE