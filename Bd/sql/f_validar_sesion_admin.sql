﻿select * from f_validar_sesion_admin('46705014','202cb962ac59075b964b07152d234b70')

CREATE OR REPLACE FUNCTION public.f_validar_sesion_admin(
    IN p_dni character,
    IN p_clave character varying)
  RETURNS TABLE(estado integer,dato character varying,dni varchar,codigo_universitario varchar,nombre_completo varchar,telefono varchar, correo varchar,id_cargo integer, cargo varchar) AS
$BODY$

declare
	v_registro record; --almacenar un registro(solo uno, si se requieren mas se usa: refcursor)
	v_respuesta character varying;
	v_estado integer;
begin
	begin
		SELECT 
		  persona.dni, 
		  persona.codigo_universitario, 
		  (persona.apellido_paterno || ' ' || persona.apellido_materno ||', '|| persona.nombres)::varchar as nombre_completo, 
		  persona.telefono, 
		  persona.correo, 
		  cargo.id_cargo, 
		  cargo.nombre as cargo, 
		  usuario.clave, 
		  usuario.estado as estado
		INTO
		  v_registro
		FROM 
		  public.usuario, 
		  public.persona, 
		  public.cargo
		WHERE 
		  persona.dni = usuario.dni AND
		  persona.id_cargo = cargo.id_cargo AND 
		  usuario.dni = p_dni AND usuario.clave =p_clave; 

		v_estado = 500; --Error


		if FOUND then
			--if v_registro.clave = md5(p_clave) then
			if v_registro.clave = p_clave then
				if v_registro.estado = 'I' then
					v_respuesta = 'Usuario Inactivo';
				else
					v_estado = 200;
					v_respuesta = v_registro.dni;
				end if;
			else
			v_respuesta = 'Usuario o Contraseña Incorrecta';
			end if;
		else
		v_respuesta = 'El usuario no existe';
		end if;
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

if v_estado = 200 then
	return query select v_estado, v_respuesta, (v_registro.dni)::varchar,(v_registro.codigo_universitario)::varchar,v_registro.nombre_completo,v_registro.telefono,v_registro.correo,v_registro.id_cargo,v_registro.cargo;
else
	return query select v_estado, v_respuesta, ''::char,''::char,''::varchar,''::varchar,''::varchar,0,''::varchar;
end if;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE