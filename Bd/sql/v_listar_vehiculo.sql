﻿select * from v_listar_vehiculo;

CREATE OR REPLACE VIEW public.v_listar_vehiculo AS 
SELECT 
  vehiculo.nro_placa, 
  vehiculo.color, 
  vehiculo.estado, 
  tipo_vehiculo.id_tipo_vehiculo, 
  tipo_vehiculo.nombre as tipo_vehiculo_nombre, 
  marca.id_marca, 
  marca.nombre as marca_nombre, 
  modelo.id_modelo, 
  modelo.nombre as modelo_nombre
FROM 
  public.vehiculo, 
  public.tipo_vehiculo, 
  public.marca, 
  public.modelo
WHERE 
  tipo_vehiculo.id_tipo_vehiculo = marca.id_tipo_vehiculo AND
  marca.id_tipo_vehiculo = modelo.id_tipo_vehiculo AND
  marca.id_marca = modelo.id_marca AND
  modelo.id_tipo_vehiculo = vehiculo.id_tipo_vehiculo AND
  modelo.id_marca = vehiculo.id_marca AND
  modelo.id_modelo = vehiculo.id_modelo;
