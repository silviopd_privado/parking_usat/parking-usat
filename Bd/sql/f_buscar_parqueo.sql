﻿select * from f_buscar_parqueo('1')

CREATE OR REPLACE FUNCTION public.f_buscar_parqueo(
    IN p_prioridad character)
  RETURNS TABLE(r_bloque character varying,r_numero integer) AS
$BODY$
	DECLARE

		v_registro record;
		
	begin	

		begin
			select * into v_registro from parqueo where prioridad = p_prioridad and estado = 'D' order by 2 limit 1;
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select v_registro.bloque::character varying,v_registro.numero;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE