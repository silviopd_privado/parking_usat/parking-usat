﻿CREATE OR REPLACE FUNCTION public.f_registro_salida(
    IN p_registro character varying,
    IN p_dni_acompanante json)
  RETURNS TABLE(resultado varchar) AS
$BODY$

declare
	v_fecha_salida date;
	v_hora_salida time without time zone;
	v_dni_acompanante_cursor refcursor;
	v_dni_acompanante_registro record;
begin
	begin

		select current_date into v_fecha_salida;
		select localtime into v_hora_salida;

		UPDATE public.registro
		   SET  fecha_salida = v_fecha_salida, 
		       hora_salida= v_hora_salida, 
		       estado= 'D'
		 WHERE id_registro = p_registro;
		
		open v_dni_acompanante_cursor for
				select
					dni
				from
					json_populate_recordset
					(
						null:: persona,
						p_dni_acompanante
					);

			loop 
				fetch v_dni_acompanante_cursor into v_dni_acompanante_registro; 
				if found then 

					UPDATE public.registro_detalle
					   SET  estado= 'S'
					 WHERE id_registro=p_registro and dni_acompanante=v_dni_acompanante_registro.dni;
					
				else
					exit; --salir del bucle
				end if;
			end loop;		
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

return query select 'SATISFACTORIO'::varchar;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE