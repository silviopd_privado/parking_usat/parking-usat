﻿select * from f_listar_registro_acompanante(1);

CREATE OR REPLACE FUNCTION public.f_listar_registro_acompanante(
    IN p_id_registro integer)
  RETURNS TABLE(id_registro integer, nro_acompanante integer,dni_acompanante varchar, nombre_completo varchar) AS
$BODY$
begin
		return query
		SELECT 
		  registro_detalle.id_registro as id,
		  registro_detalle.acompanantes + 1, 
		  (persona.dni)::varchar, 
		  (persona.apellido_paterno||' '||persona.apellido_materno||', '|| persona.nombres)::varchar as nombres
		FROM 
		  public.registro_detalle, 
		  public.persona
		WHERE 
		  registro_detalle.dni_acompanante = persona.dni AND registro_detalle.id_registro = p_id_registro
		ORDER BY registro_detalle.acompanantes;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE