﻿select * from f_buscar_vehiculo('666666')

CREATE OR REPLACE FUNCTION public.f_buscar_vehiculo(IN p_nro_placa character varying)
  RETURNS TABLE(resultado integer) AS
$BODY$

declare
	v_resultado integer;
	v_nro_placa varchar;
begin
	begin

		select nro_placa into v_nro_placa from vehiculo where nro_placa=p_nro_placa;
		if v_nro_placa is null then
			v_resultado=1;
		else
			v_resultado=0;
		end if;
		
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

return query select v_resultado;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE