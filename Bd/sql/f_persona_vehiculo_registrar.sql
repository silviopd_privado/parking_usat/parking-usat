﻿CREATE OR REPLACE FUNCTION public.f_persona_vehiculo_registrar(
    IN p_dni varchar,
    IN p_codigo_universitario varchar,
    IN p_apellido_paterno character varying,
    IN p_apellido_materno character varying,
    IN p_nombres character varying,
    IN p_telefono character varying,
    IN p_correo character varying,
    IN p_id_cargo integer,
    IN p_id_zona varchar,
    IN p_nro_placa varchar,
    IN p_id_tipo_vehiculo integer,
    IN p_id_marca integer,
    IN p_id_modelo integer,
    IN p_color character varying,
    IN p_fotodnia character varying,
    IN p_fotodnib character varying,
    IN p_fotocarroa character varying,
    IN p_fotocarrob character varying,
    IN p_fotocarroc character varying,
    IN p_fotocarrod character varying)
  RETURNS TABLE(estado integer, r_dni character varying, r_nro_placa character varying) AS
$BODY$

declare
	v_persona_vehiculo_dni character varying;
	v_dni character varying;
	v_nro_placa character varying;
	v_estado integer;
	v_documento integer;
begin
	begin

		v_estado=200;
		SELECT dni INTO v_persona_vehiculo_dni FROM documento WHERE dni = p_dni and nro_placa = p_nro_placa;
		if v_persona_vehiculo_dni is not null then
			v_estado=500;
			RAISE EXCEPTION 'YA EXISTE UN REGISTRO CON DNI (%) Y NRO_PLACA (%)',p_dni,p_nro_placa;
		end if;

		select dni into v_dni from persona where dni = p_dni;
		if v_dni is null then 
			INSERT INTO public.persona(
				dni, 
				codigo_universitario, 
				apellido_paterno, 
				apellido_materno, 
				nombres, 
				telefono, 
				correo, 
				id_cargo, 
				id_zona)
			VALUES (
				p_dni, 
				p_codigo_universitario, 
				p_apellido_paterno, 
				p_apellido_materno, 
				p_nombres, 
				p_telefono, 
				p_correo, 
				p_id_cargo, 
				p_id_zona);

			INSERT INTO public.fotos_persona(
				dni, 
				lado, 
				ruta)
			VALUES (
				p_dni, 
				'A', 
				p_fotodnia);


			INSERT INTO public.fotos_persona(
				dni, 
				lado, 
				ruta)
			VALUES (
				p_dni, 
				'B', 
				p_fotodnib);
		end if;

		
		select nro_placa into v_nro_placa from vehiculo where nro_placa = p_nro_placa;
		if v_nro_placa is null then
			INSERT INTO public.vehiculo(
				nro_placa, 
				color,  
				id_tipo_vehiculo, 
				id_marca, 
				id_modelo)
			VALUES (
				p_nro_placa, 
				p_color,  
				p_id_tipo_vehiculo, 
				p_id_marca, 
				p_id_modelo);

			INSERT INTO public.fotos_vehiculo(
				nro_placa, 
				lado, 
				ruta)
			VALUES (
				p_nro_placa, 
				'A', 
				p_fotocarroa);


			INSERT INTO public.fotos_vehiculo(
				nro_placa, 
				lado, 
				ruta)
			VALUES (
				p_nro_placa, 
				'B', 
				p_fotocarrob);


			INSERT INTO public.fotos_vehiculo(
				nro_placa, 
				lado, 
				ruta)
			VALUES (
				p_nro_placa, 
				'C', 
				p_fotocarroc);


			INSERT INTO public.fotos_vehiculo(
				nro_placa, 
				lado, 
				ruta)
			VALUES (
				p_nro_placa, 
				'D', 
				p_fotocarrod);
		end if;

		if v_persona_vehiculo_dni is null then
			select * into v_documento from f_generar_correlativo('documento');
						
			INSERT INTO public.documento(
				id_documento, 
				ruta, 
				nro_placa, 
				dni)
			VALUES (
				v_documento, 
				'', 
				p_nro_placa, 
				p_dni);

			update correlativo set numero = v_documento where tabla = 'documento';
		end if;
	
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

if v_estado = 200 then
	return query select v_estado, p_dni::varchar,p_nro_placa::varchar;
else
	return query select v_estado, ''::varchar,''::varchar;
end if;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE