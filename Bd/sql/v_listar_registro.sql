﻿select * from v_listar_registro
CREATE OR REPLACE VIEW public.v_listar_registro AS 
 SELECT DISTINCT registro.id_registro,
    ((((date_part('day'::text, registro.fecha_entrada) || '/'::text) || date_part('month'::text, registro.fecha_entrada)) || '/'::text) || date_part('year'::text, registro.fecha_entrada))::character varying AS fecha_entrada,
    (((date_part('hour'::text, registro.hora_entrada) || ':'::text) ||
        CASE
            WHEN date_part('minute'::text, registro.hora_entrada) < 10::double precision THEN '0'::text
            ELSE ''::text
        END) || date_part('minute'::text, registro.hora_entrada))::character varying AS hora_entrada,
        CASE
            WHEN registro.motivo IS NULL THEN '-'::character varying
            WHEN registro.motivo::text = ''::text THEN '-'::character varying
            ELSE registro.motivo
        END AS motivo,
        CASE
            WHEN char_length(registro.motivo::text) = 3 THEN (( SELECT zona_1.nombre
               FROM zona zona_1
              WHERE zona_1.id_zona = registro.motivo::bpchar))::text
            WHEN char_length(registro.motivo::text) = 8 THEN ( SELECT (((persona_1.apellido_paterno::text || ' '::text) || persona_1.apellido_materno::text) || ', '::text) || persona_1.nombres::text
               FROM persona persona_1
              WHERE persona_1.dni = registro.motivo::bpchar)
            ELSE
		'-'
        END::character varying AS nombre_motivo,
    registro.bloque,
    registro.numero,
    registro.nro_placa,
    registro.estado,
        CASE
            WHEN registro.id_motivo::text = '1'::text THEN 'SI'::text
            ELSE 'NO'::text
        END AS id_visitante,
        CASE
            WHEN registro.motivo_descripcion IS NULL THEN '-'::character varying
            WHEN registro.motivo_descripcion::text = ''::text THEN '-'::character varying
            ELSE registro.motivo_descripcion
        END AS motivo_descripcion,
        CASE
            WHEN registro.codigo_visitante IS NULL THEN '-'::character varying
            WHEN registro.codigo_visitante::text = ''::text THEN '-'::character varying
            ELSE registro.codigo_visitante
        END AS codigo_visitante,
    persona.dni,
    ((((persona.apellido_paterno::text || ' '::text) || persona.apellido_materno::text) || ', '::text) || persona.nombres::text)::character varying AS nombres
   FROM registro,
    persona,
    zona
  WHERE registro.dni = persona.dni
  ORDER BY registro.id_registro;