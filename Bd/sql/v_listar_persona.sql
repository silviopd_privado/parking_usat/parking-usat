﻿select * from v_listar_persona;

CREATE OR REPLACE VIEW public.v_listar_persona AS 
SELECT 
  persona.dni, 
  persona.codigo_universitario, 
  (persona.apellido_paterno ||' '|| persona.apellido_materno||', '|| persona.nombres)::varchar as nombre_completo, 
  persona.telefono, 
  persona.correo, 
  persona.estado, 
  cargo.id_cargo, 
  cargo.nombre as cargo_nombre, 
  zona.id_zona, 
  zona.nombre as zona_nombre
FROM 
  public.persona, 
  public.cargo, 
  public.zona
WHERE 
  persona.id_cargo = cargo.id_cargo AND
  persona.id_zona = zona.id_zona;
