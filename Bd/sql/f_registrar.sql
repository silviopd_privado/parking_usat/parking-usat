﻿select * from f_registrar('46705019',1,'000','123456',1,1,1,1,'EAO','espiar','46705014','[{"dni":"12121215","id_cargo":1,"id_zona":"003"},{"dni":"12121216","id_cargo":1,"id_zona":"003"}]');

CREATE OR REPLACE FUNCTION public.f_registrar(
    IN p_dni character varying,
    IN p_id_cargo integer,
    IN p_id_zona varchar,
    IN p_nro_placa character varying, 
    IN p_id_tipo_vehiculo integer,
    IN p_id_marca integer,
    IN p_id_modelo integer,   
    IN p_id_motivo integer,
    IN p_motivo character varying,
    IN p_motivo_descripcion character varying,
    IN p_dni_usuario varchar,
    IN p_dni_acompanante json)
  RETURNS TABLE(r_id_registro integer,r_dni varchar, r_nro_placa varchar,r_codigo_visitante varchar) AS
$BODY$

declare
	v_id_registro integer;
	v_parking record;
	v_zona varchar;
	v_acompanantes integer;
	v_codigo_visitante varchar;
	v_tipo_vehiculo integer;
	v_id_motivo integer;
	v_dni_acompanante varchar;
	v_dni varchar;
	
	v_dni_acompanante_cursor refcursor;
	v_dni_acompanante_registro record;
begin
	begin

		

		select * into v_id_registro from f_generar_correlativo('registro');

		select dni into v_dni from persona where dni = p_dni;
		if v_dni is null then
			INSERT INTO public.persona(dni, id_cargo, id_zona)
			VALUES (p_dni,p_id_cargo,p_id_zona);
		end if;
				
		--3(administradores) y 2(profesores) cargo
		select id_cargo into v_id_motivo from persona where dni = p_dni;
		if v_id_motivo is not null then
			if v_id_motivo  = 3 or v_id_motivo  = 4 then
				v_id_motivo=1;
		        else
				v_id_motivo=2;
		        end if;  
		end if;

		select id_tipo_vehiculo into v_tipo_vehiculo from vehiculo where nro_placa = p_nro_placa;
		if v_tipo_vehiculo is null then
			INSERT INTO public.vehiculo(nro_placa, id_tipo_vehiculo, id_marca, id_modelo)
			VALUES (p_nro_placa,p_id_tipo_vehiculo, p_id_marca, p_id_modelo);
		end if;

		v_tipo_vehiculo = p_id_tipo_vehiculo;	
		
		--1 (administrativo y docente)
		--2 (alumno y visitante)
		select * into v_parking from f_parking(v_id_motivo,v_tipo_vehiculo);		

		--id_motivo => 1(zona) y 2(profesor)
		--motivo => codigo
		--descripcion => descripcion
		INSERT INTO public.registro(
			id_registro,  
			motivo, 
			bloque, 
			numero, 
			nro_placa, 
			dni, 
			motivo_descripcion, 
			id_motivo,
			dni_usuario)
		VALUES (
			v_id_registro,
			p_motivo,
			v_parking.bloque,
			v_parking.numero,
			p_nro_placa,
			p_dni,
			p_motivo_descripcion,			
			p_id_motivo,
			p_dni_usuario
		);

		--id_motivo
			--0(Estudiante/profesor/Administrativo)
			--1(Visitante)
		if p_id_motivo  = 1 then

			v_zona = p_motivo;

			if length(p_motivo)  = 8 then
				select id_zona into v_zona from persona where dni=p_motivo;
			end if; 

			v_codigo_visitante=v_id_registro||'-'||v_zona;

			UPDATE public.registro SET  codigo_visitante=v_codigo_visitante WHERE id_registro=v_id_registro;
		end if;

		v_acompanantes=0;

		open v_dni_acompanante_cursor for
				select
					dni,
					id_cargo,
					id_zona
				from
					json_populate_recordset
					(
						null:: persona,
						p_dni_acompanante
					);

			loop 
				fetch v_dni_acompanante_cursor into v_dni_acompanante_registro; 
				if found then 

					select dni into v_dni_acompanante from persona where dni=v_dni_acompanante_registro.dni;
					if v_dni_acompanante is null then		
						INSERT INTO public.persona(dni, id_cargo, id_zona)
						VALUES (v_dni_acompanante_registro.dni,v_dni_acompanante_registro.id_cargo,v_dni_acompanante_registro.id_zona);
					end if;
				
					INSERT INTO public.registro_detalle(
						id_registro,
						acompanantes, 
						dni_acompanante)
					VALUES (
						v_id_registro,
						v_acompanantes,
						v_dni_acompanante_registro.dni);

					v_acompanantes=v_acompanantes+1;	
					
				else
					exit; --salir del bucle
				end if;
			end loop;		

			UPDATE public.correlativo SET  numero=v_id_registro WHERE tabla='registro';
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

return query select v_id_registro,p_dni,p_nro_placa,v_codigo_visitante::varchar;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE