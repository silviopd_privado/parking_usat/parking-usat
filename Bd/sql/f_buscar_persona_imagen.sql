﻿select * from f_buscar_persona_imagen('98237472')

CREATE OR REPLACE FUNCTION public.f_buscar_persona_imagen(IN p_dni character varying)
  RETURNS TABLE(resultado integer,r_foto character varying) AS
$BODY$

declare
	v_resultado integer;
	v_foto varchar;
begin
	begin

		select foto into v_foto from persona where dni=p_dni;
		if v_foto is null then
			v_foto = '0';
			v_resultado = 0;
		else
			v_resultado = 1;	
		end if;
		
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

return query select v_resultado,v_foto;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
