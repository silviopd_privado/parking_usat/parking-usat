﻿select * from f_parking(1,1)

CREATE OR REPLACE FUNCTION public.f_parking(
    IN p_prioridad integer,
    IN p_tipo_vehiculo integer)
  RETURNS TABLE(bloque varchar,numero integer) AS
$BODY$

declare
	v_registro record; --almacenar un registro(solo uno, si se requieren mas se usa: refcursor)
	v_estado integer;
begin
	begin
		select * into v_registro from parking where estado = 'A' and prioridad=p_prioridad and tipo_vehiculo=p_tipo_vehiculo order by 1,2 limit 1;

		if v_registro.numero is not null then
			v_estado=200;
		else
			v_estado=500;
		end if;

		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

if v_estado = 200 then
	return query select v_registro.bloque::varchar,v_registro.numero;
else
	return query select ''::varchar,0;
end if;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE