<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CARNET</title>	
	<link rel="stylesheet" type="text/css" href="../util/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
		svg,img,canvas{
			width: 300px;
		}
		body {
		    display: inline-block;
		    margin: 0px auto;
		    text-align: center;
		}
		h6{
			font-weight: bold;
		}
	</style>	
</head>
<body>
<br>
<div class="container" id="editor">
	<div class="row">
		<div class="col"><h6>DNI</h6></div>
		<div class="col"><h6><?php echo $_GET["dni"] ?></h6></div>
	</div>
	<div class="row">
		<div class="col"><h6>PLACA</h6></div>
		<div class="col"><h6><?php echo $_GET["placa"] ?></h6></div>		
	</div>	
	<canvas id="barcode"></canvas>
</div>	
<button onclick="descargar()" id="btn">DESCARGAR CARNET</button>

	<script type="text/javascript" src="../util/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../util/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../util/barcode39/JsBarcode.all.min.js"></script>
	<script type="text/javascript" src="../util/jspdf/jspdf.debug.js"></script>

	<script type="text/javascript">
		$(document).ready(function($) {
			JsBarcode("#barcode", "<?php echo $_GET["dni"].$_GET["placa"] ?>" , {
			displayValue: "false"
			});
		});

		function descargar(){
			$("#btn").hide();

			var pdf = new jsPDF('p','pt','a4');

			pdf.addHTML(document.body,function() {
			    //pdf.output('datauri');
			    pdf.save('carnet.pdf');
			});
		}
	</script>
</body>
</html>	
