/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
  var navListItems = $("div.setup-panel div a"),
    allWells = $(".setup-content"),
    allNextBtn = $(".nextBtn");

  allWells.hide();

  navListItems.click(function(e) {
    e.preventDefault();
    var $target = $($(this).attr("href")),
      $item = $(this);

    if (!$item.hasClass("disabled")) {
      navListItems.removeClass("btn-primary").addClass("btn-default");
      $item.addClass("btn-primary");
      allWells.hide();
      $target.show();
      $target.find("input:eq(0)").focus();
    }
  });

  allNextBtn.click(function() {
    var curStep = $(this).closest(".setup-content"),
      curStepBtn = curStep.attr("id"),
      nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]')
        .parent()
        .next()
        .children("a"),
      curInputs = curStep.find("input[type='tel'],input[type='url']"),
      isValid = true;

    $(".form-group").removeClass("has-error");
    for (var i = 0; i < curInputs.length; i++) {
      if (!curInputs[i].validity.valid) {
        isValid = false;
        $(curInputs[i])
          .closest(".form-group")
          .addClass("has-error");
      }
    }

    if (isValid) nextStepWizard.removeAttr("disabled").trigger("click");
  });

  $("div.setup-panel div a.btn-primary").trigger("click");

  $("#txtdni").val("");
  $("#txtcodigouniversitario").val("");
  $("#txtapellidopaterno").val("");
  $("#txtapellidomaterno").val("");
  $("#txtnombres").val("");
  $("#txttelefono").val("");
  $("#txtcorreo").val("");
  $("#cbocargo").val("");
  $("#txtnroplaca").val("");
  $("#cbotipovehiculo").val("");
  $("#cbomarca").val("");
  $("#cbomodelo").val("");
  $("#cbocolor").val("");

  document
    .querySelector("#link")
    .setAttribute("href", DIRECCION_WS + "terminos_y_condiciones.php");

  cargarComboCargo("#cbocargo", "seleccione");
  cargarComboTipoVehiculo("#cbotipovehiculo", "seleccione");

  cargarComboZona("#cbozona", "seleccione");

  $("#cbozona").attr("disabled", true);
  /*
    $("#barcode").JsBarcode("DORA",{
        lineColor: "#0aa",
        displayValue: false
    });
    */
});

$("#cbocargo").change(function() {
  var id_cargo = $("#cbocargo").val();
  if (id_cargo == 1) {
    $("#cbozona").val("002");
    $("#cbozona").attr("disabled", true);
  } else {
    $("#cbozona").val("");
    $("#cbozona").attr("disabled", false);
    //deshabilitar una opcion
    //$("select option:contains('ESTUDIANTE')").attr("disabled","disabled");

    //esconder una opcion
    $("#cbozona option[value=002]").hide();
  }
});

$("#cbotipovehiculo").change(function() {
  var id_tipo_vehiculo = $("#cbotipovehiculo").val();
  cargarComboMarca("#cbomarca", "seleccione", id_tipo_vehiculo);
});

$("#cbomarca").change(function() {
  var id_tipo_vehiculo = $("#cbotipovehiculo").val();
  var id_marca = $("#cbomarca").val();
  cargarComboModelo("#cbomodelo", "seleccione", id_tipo_vehiculo, id_marca);
});

$("#frmgrabar").submit(function(evento) {
  evento.preventDefault();

  swal(
    {
      title: "Confirme",
      text: "¿Esta seguro de registrarse?",
      showCancelButton: true,
      confirmButtonColor: "#3d9205",
      confirmButtonText: "Si",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: true,
      imageUrl: "../imagenes/pregunta.png"
    },
    function(isConfirm) {
      if (isConfirm) {
        //el usuario hizo clic en el boton SI

        var ruta = DIRECCION_WS + "persona_vehiculo.reg.agregar.php";

        var dni = $("#txtdni").val();
        var codigo_universitario = $("#txtcodigouniversitario").val();
        var apellido_paterno = $("#txtapellidopaterno").val();
        var apellido_materno = $("#txtapellidomaterno").val();
        var nombres = $("#txtnombres").val();
        var telefono = $("#txttelefono").val();
        var correo = $("#txtcorreo").val();
        var id_cargo = $("#cbocargo").val();
        var id_zona = $("#cbozona").val();
        var nro_placa = $("#txtnumeroplaca").val();
        var id_tipo_vehiculo = $("#cbotipovehiculo").val();
        var id_marca = $("#cbomarca").val();
        var id_modelo = $("#cbomodelo").val();
        var color = $("#txtcolor").val();
        var foto = $("#foto")[0].files[0];
        var fotodnia = $("#fotodnia")[0].files[0];
        var fotodnib = $("#fotodnib")[0].files[0];
        var fotocarroa = $("#fotocarroa")[0].files[0];
        var fotocarrob = $("#fotocarrob")[0].files[0];
        var fotocarroc = $("#fotocarroc")[0].files[0];
        var fotocarrod = $("#fotocarrod")[0].files[0];

        var form_data = new FormData();
        form_data.append("dni", dni);
        form_data.append("codigo_universitario", codigo_universitario);
        form_data.append("apellido_paterno", apellido_paterno);
        form_data.append("apellido_materno", apellido_materno);
        form_data.append("nombres", nombres);
        form_data.append("telefono", telefono);
        form_data.append("correo", correo);
        form_data.append("id_cargo", id_cargo);
        form_data.append("id_zona", id_zona);
        form_data.append("nro_placa", nro_placa);
        form_data.append("id_tipo_vehiculo", id_tipo_vehiculo);
        form_data.append("id_marca", id_marca);
        form_data.append("id_modelo", id_modelo);
        form_data.append("color", color);
        form_data.append("foto", foto);
        form_data.append("fotodnia", fotodnia);
        form_data.append("fotodnib", fotodnib);
        form_data.append("fotocarroa", fotocarroa);
        form_data.append("fotocarrob", fotocarrob);
        form_data.append("fotocarroc", fotocarroc);
        form_data.append("fotocarrod", fotocarrod);

        console.log(form_data);

        $.ajax({
          url: ruta,
          type: "POST",
          data: form_data,
          cache: false,
          enctype: "multipart/form-data",
          processData: false,
          contentType: false,
          success: function(data) {
            swal("Exito", "Se Agrego Correctamente", "success");
            $("#btncerrar").click(); //cerrar ventana
            document.getElementById('link').click();

            var win = window.open(DIRECCION_WS_CARNET+'carnet.php?dni='+dni+'&placa='+nro_placa, '_blank');
            //win.focus();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
          }
        });
      }
    }
  );
});
