<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Persona.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];

try {

    $obj = new Persona();
    $obj->setDni($dni);
    $resultado = $obj->buscarPersonaImagen();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "resultado" => $resultado[$i]["resultado"],
            "foto" => $resultado[$i]["r_foto"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}