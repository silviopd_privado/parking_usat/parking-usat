<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Persona.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Persona();
    $resultado = $obj->listarPersona();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "codigo_universitario" => $resultado[$i]["codigo_universitario"],
            "nombre_completo" => $resultado[$i]["nombre_completo"],
            "telefono" => $resultado[$i]["telefono"],
            "correo" => $resultado[$i]["correo"],
            "estado" => $resultado[$i]["estado"],
            "id_cargo" => $resultado[$i]["id_cargo"],
            "cargo_nombre" => $resultado[$i]["cargo_nombre"],
            "id_zona" => $resultado[$i]["id_zona"],
            "zona_nombre" => $resultado[$i]["zona_nombre"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}