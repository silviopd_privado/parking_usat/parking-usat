<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Persona_Vehiculo.clase.php';
require_once '../util/funciones/Funciones.clase.php';

if (!isset($_POST["dni"]) || !isset($_POST["codigo_universitario"]) ||
        !isset($_POST["apellido_paterno"]) || !isset($_POST["apellido_materno"]) ||
        !isset($_POST["nombres"]) || !isset($_POST["telefono"]) ||
        !isset($_POST["correo"]) || !isset($_POST["id_cargo"]) ||
        !isset($_POST["id_zona"]) || !isset($_POST["nro_placa"]) ||
        !isset($_POST["id_tipo_vehiculo"]) || !isset($_POST["id_marca"]) ||
        !isset($_POST["id_modelo"]) || !isset($_POST["color"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$dni = $_POST["dni"];
$codigo_universitario = $_POST["codigo_universitario"];
$apellido_paterno = $_POST["apellido_paterno"];
$apellido_materno = $_POST["apellido_materno"];
$nombres = $_POST["nombres"];
$telefono = $_POST["telefono"];
$correo = $_POST["correo"];
$id_cargo = $_POST["id_cargo"];
$id_zona = $_POST["id_zona"];
$nro_placa = $_POST["nro_placa"];
$id_tipo_vehiculo = $_POST["id_tipo_vehiculo"];
$id_marca = $_POST["id_marca"];
$id_modelo = $_POST["id_modelo"];
$color = $_POST["color"];
//$fotodnia = $_POST["fotodnia"];
//$fotodnib = $_POST["fotodnib"];
//$fotocarroa = $_POST["fotocarroa"];
//$fotocarrob = $_POST["fotocarrob"];
//$fotocarroc = $_POST["fotocarroc"];
//$fotocarrod = $_POST["fotocarrod"];


try {

    $obj = new Persona_Vehiculo();
    $obj->setDni($dni);
    $obj->setCodigo_universitario($codigo_universitario);
    $obj->setApellido_paterno($apellido_paterno);
    $obj->setApellido_materno($apellido_materno);
    $obj->setNombres($nombres);
    $obj->setTelefono($telefono);
    $obj->setCorreo($correo);
    $obj->setId_cargo($id_cargo);
    $obj->setId_zona($id_zona);
    $obj->setNro_placa($nro_placa);
    $obj->setId_tipo_vehiculo($id_tipo_vehiculo);
    $obj->setId_marca($id_marca);
    $obj->setId_modelo($id_modelo);
    $obj->setColor($color);
    $obj->setFoto("");
    $obj->setFotodnia("");
    $obj->setFotodnib("");
    $obj->setFotocarroa("");
    $obj->setFotocarrob("");
    $obj->setFotocarroc("");
    $obj->setFotocarrod("");
    $resultado = $obj->agregarRegistrar();

    if ($resultado) {

        /* imagen */
        if ($_FILES['foto']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['foto']['name'];
            $tmp_dir = $_FILES['foto']['tmp_name'];
            $imgSize = $_FILES['foto']['size'];

            $upload_dir = '../imagenes/personas/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $dni . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updatePersonaRegistrar2($dni, $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            throw new UploadException($_FILES['file']['error']);
        }
        /* imagen */

        /* imagen */
        if ($_FILES['fotodnia']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['fotodnia']['name'];
            $tmp_dir = $_FILES['fotodnia']['tmp_name'];
            $imgSize = $_FILES['fotodnia']['size'];

            $upload_dir = '../imagenes/personas/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $dni . "A" . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updatePersonaRegistrar($dni, 'A', $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            throw new UploadException($_FILES['file']['error']);
        }
        /* imagen */

        /* imagen */
        if ($_FILES['fotodnib']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['fotodnib']['name'];
            $tmp_dir = $_FILES['fotodnib']['tmp_name'];
            $imgSize = $_FILES['fotodnib']['size'];

            $upload_dir =  '../imagenes/personas/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $dni . "B" . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updatePersonaRegistrar($dni, 'B', $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            throw new UploadException($_FILES['file']['error']);
        }
        /* imagen */

        /* imagen */
        if ($imgSize = $_FILES['fotocarroa']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['fotocarroa']['name'];
            $tmp_dir = $_FILES['fotocarroa']['tmp_name'];
            $imgSize = $_FILES['fotocarroa']['size'];

            $upload_dir = '../imagenes/vehiculos/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $nro_placa . "A" . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updateVehiculoRegistrar($nro_placa, 'A', $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            throw new UploadException($_FILES['file']['error']);
        }
        /* imagen */

        /* imagen */
        if ($imgSize = $_FILES['fotocarrob']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['fotocarrob']['name'];
            $tmp_dir = $_FILES['fotocarrob']['tmp_name'];
            $imgSize = $_FILES['fotocarrob']['size'];

            $upload_dir = '../imagenes/vehiculos/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $nro_placa . "B" . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updateVehiculoRegistrar($nro_placa, 'B', $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            throw new UploadException($_FILES['file']['error']);
        }
        /* imagen */

        /* imagen */
        if ($imgSize = $_FILES['fotocarroc']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['fotocarroc']['name'];
            $tmp_dir = $_FILES['fotocarroc']['tmp_name'];
            $imgSize = $_FILES['fotocarroc']['size'];

            $upload_dir = '../imagenes/vehiculos/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $nro_placa . "C" . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updateVehiculoRegistrar($nro_placa, 'C', $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            throw new UploadException($_FILES['file']['error']);
        }
        /* imagen */

        /* imagen */
        if ($imgSize = $_FILES['fotocarrod']['error'] === UPLOAD_ERR_OK) { // subido exitosamente http://php.net/manual/es/features.file-upload.errors.php
            $imgFile = $_FILES['fotocarrod']['name'];
            $tmp_dir = $_FILES['fotocarrod']['tmp_name'];
            $imgSize = $_FILES['fotocarrod']['size'];

            $upload_dir = '../imagenes/vehiculos/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $userpic = $nro_placa . "D" . "." . $imgExt;

            // allow valid image file formats
            if (in_array($imgExt, $valid_extensions)) {
                // Check file size '5MB'
                if ($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir, $upload_dir . $userpic);
                    $obj->updateVehiculoRegistrar($nro_placa, 'D', $userpic);
                } else {
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        }
        /* imagen */
    } else {
        throw new UploadException($_FILES['file']['error']);
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
