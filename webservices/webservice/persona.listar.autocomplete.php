<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Persona.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Persona();
    $resultado = $obj->listarPersonaAutocomplete();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "nombre_completo" => $resultado[$i]["nombre_completo"],
            "id_zona" => $resultado[$i]["id_zona"],
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}