<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Zona.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Zona();
    $resultado = $obj->listarRegistrar();

    $listadezonas = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_zona" => $resultado[$i]["id_zona"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listadezonas[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadezonas);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}