<?php

header('Access-Control-Allow-Origin: *'); 

require_once '../negocio/Cargo.clase.php'; 
require_once '../util/funciones/Funciones.clase.php'; 

try {

    $obj = new Cargo(); 
    $resultado = $obj -> listarRegistrar(); 

    $listadepartamento = array(); 
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_cargo" => $resultado[$i]["id_cargo"], 
            "nombre" => $resultado[$i]["nombre"]); 

        $listadepartamento[$i] = $datos; 
    }
    Funciones::imprimeJSON(200, "", $listadepartamento); 
}catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc -> getMessage(), ""); 
}