<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Vehiculo.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Vehiculo();
    $resultado = $obj->listarVehiculo();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "nro_placa" => $resultado[$i]["nro_placa"],
            "color" => $resultado[$i]["color"],
            "estado" => $resultado[$i]["estado"],
            "id_tipo_vehiculo" => $resultado[$i]["id_tipo_vehiculo"],
            "tipo_vehiculo_nombre" => $resultado[$i]["tipo_vehiculo_nombre"],
            "id_marca" => $resultado[$i]["id_marca"],
            "marca_nombre" => $resultado[$i]["marca_nombre"],
            "id_modelo" => $resultado[$i]["id_modelo"],
            "modelo_nombre" => $resultado[$i]["modelo_nombre"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}