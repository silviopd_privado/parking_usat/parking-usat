<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Registro.clase.php';
require_once '../util/funciones/Funciones.clase.php';

if (!isset($_POST["id_registro"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$id_registro = $_POST["id_registro"];

try {

    $obj = new Registro(); 
    $obj->setId_registro($id_registro);
    $resultado = $obj -> listarRegistrarDetalle(); 

    $listadepartamento = array(); 
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_registro" => $resultado[$i]["id_registro"], 
            "nro_acompanante" => $resultado[$i]["nro_acompanante"],
            "dni_acompanante" => $resultado[$i]["dni_acompanante"], 
            "nombre_completo" => $resultado[$i]["nombre_completo"]);

        $listadepartamento[$i] = $datos; 
    }
    Funciones::imprimeJSON(200, "", $listadepartamento); 
}catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc -> getMessage(), ""); 
}