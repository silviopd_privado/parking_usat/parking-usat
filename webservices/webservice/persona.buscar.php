<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Persona.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];

try {

    $obj = new Persona();
    $obj->setDni($dni);
    $resultado = $obj->buscarPersona();

    if ($resultado) {
        Funciones::imprimeJSON(200, $dni, $resultado);
    }else{
        Funciones::imprimeJSON(200, "Ocurrio un error", $resultado);
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
