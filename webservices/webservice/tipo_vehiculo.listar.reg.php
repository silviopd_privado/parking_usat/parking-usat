<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Tipo_Vehiculo.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Tipo_Vehiculo();
    $resultado = $obj->listarRegistrar();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_tipo_vehiculo" => $resultado[$i]["id_tipo_vehiculo"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}