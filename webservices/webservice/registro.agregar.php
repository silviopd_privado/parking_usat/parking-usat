<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Registro.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];
$id_cargo = $_POST["id_cargo"];
$id_zona = $_POST["id_zona"];
$nro_placa = $_POST["nro_placa"];
$id_tipo_vehiculo = $_POST["id_tipo_vehiculo"];
$id_marca = $_POST["id_marca"];
$id_modelo = $_POST["id_modelo"];
$id_motivo = $_POST["id_motivo"];
$motivo = $_POST["motivo"];
$motivo_descripcion = $_POST["motivo_descripcion"];
$dni_usuario = $_POST["dni_usuario"];
$dni_acompanante = $_POST["dni_acompanante"];

try {

    $obj = new Registro();
    $obj->setDni($dni);
    $obj->setId_cargo($id_cargo);
    $obj->setId_zona($id_zona);
    $obj->setNro_placa($nro_placa);
    $obj->setId_tipo_vehiculo($id_tipo_vehiculo);
    $obj->setId_marca($id_marca);
    $obj->setId_modelo($id_modelo);
    $obj->setId_motivo($id_motivo);
    $obj->setMotivo($motivo);
    $obj->setMotivo_descripcion($motivo_descripcion);
    $obj->setDni_usuario($dni_usuario);
    $obj->setDni_acompanante($dni_acompanante);
    $resultado = $obj->agregarRegistro();

    $listaregistro= array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "estado" => $resultado[$i]["r_estado"],
            "id_registro" => $resultado[$i]["r_id_registro"],
            "dni" => $resultado[$i]["r_dni"],
            "nro_placa" => $resultado[$i]["r_nro_placa"],
            "codigo_visitante" => $resultado[$i]["r_codigo_visitante"],
            "bloque" => $resultado[$i]["r_bloque"],
            "numero" => $resultado[$i]["r_numero"]
        );

        $listaregistro[$i] = $datos;
    }

     $estado = $resultado[0]["r_estado"];
     
     if ($estado==200) {
         Funciones::imprimeJSON(200, "REGISTRO SATISFACTORIO", $listaregistro);
     } else  if ($estado==300){
         Funciones::imprimeJSON(200, "YA SE ENCUENTRA REGISTRADO", $listaregistro);
     }else  if ($estado==400){
        Funciones::imprimeJSON(200, "ESTABCIONAMIENTO LLENO", $listaregistro);
     }else{
        Funciones::imprimeJSON(200, "OCURRIO UN ERROR","");
     }
      
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
