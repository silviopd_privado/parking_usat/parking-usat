<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Vehiculo.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$nro_placa = $_POST["nro_placa"];

try {

    $obj = new Vehiculo();
    $obj->setNro_placa($nro_placa);
    $resultado = $obj->buscarVehiculo();

    if ($resultado) {
        Funciones::imprimeJSON(200, "Busqueda Satisfactoria", $resultado);
    }else{
        Funciones::imprimeJSON(200, "Ocurrio un error", $resultado);
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
