<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Registro.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $id_registro = $_POST["id_registro"];
    
    $obj = new Registro();
    $obj->setId_registro($id_registro);
    
    $resultado = $obj->registroDarAlta();

    Funciones::imprimeJSON(200, "", "");
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}