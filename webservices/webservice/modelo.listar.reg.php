<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Modelo.clase.php';
require_once '../util/funciones/Funciones.clase.php';


if (!isset($_POST["id_tipo_vehiculo"]) || !isset($_POST["id_marca"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$id_tipo_vehiculo = $_POST["id_tipo_vehiculo"];
$id_marca = $_POST["id_marca"];

try {
        $obj = new Modelo();
        $resultado = $obj->listarRegistrar($id_tipo_vehiculo, $id_marca);

        $listadistrito = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_tipo_vehiculo" => $resultado[$i]["id_tipo_vehiculo"],
                "id_marca" => $resultado[$i]["id_marca"],
                "id_modelo" => $resultado[$i]["id_modelo"],
                "nombre" => $resultado[$i]["nombre"]            
            );

            $listadistrito[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listadistrito);
    
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}