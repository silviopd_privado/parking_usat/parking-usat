<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Marca.clase.php';
require_once '../util/funciones/Funciones.clase.php';


if (!isset($_POST["id_tipo_vehiculo"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$id_tipo_vehiculo = $_POST["id_tipo_vehiculo"];

try {
        $obj = new Marca();
        $resultado = $obj->listarRegistrar($id_tipo_vehiculo);

        $listaprovincia = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_tipo_vehiculo" => $resultado[$i]["id_tipo_vehiculo"],
                "id_marca" => $resultado[$i]["id_marca"],
                "nombre" => $resultado[$i]["nombre"]                
            );

            $listaprovincia[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaprovincia);
    
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}