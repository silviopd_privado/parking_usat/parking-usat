<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Registro.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Registro(); 
    $resultado = $obj -> listarRegistrar(); 

    $listadepartamento = array(); 
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_registro" => $resultado[$i]["id_registro"], 
            "fecha_entrada" => $resultado[$i]["fecha_entrada"],
            "hora_entrada" => $resultado[$i]["hora_entrada"], 
            "motivo" => $resultado[$i]["motivo"],
            "nombre_motivo" => $resultado[$i]["nombre_motivo"], 
            "bloque" => $resultado[$i]["bloque"],
            "numero" => $resultado[$i]["numero"], 
            "nro_placa" => $resultado[$i]["nro_placa"],
            "estado" => $resultado[$i]["estado"], 
            "id_visitante" => $resultado[$i]["id_visitante"],
            "motivo_descripcion" => $resultado[$i]["motivo_descripcion"], 
            "codigo_visitante" => $resultado[$i]["codigo_visitante"],
            "dni" => $resultado[$i]["dni"], 
            "nombres" => $resultado[$i]["nombres"]);

        $listadepartamento[$i] = $datos; 
    }
    Funciones::imprimeJSON(200, "", $listadepartamento); 
}catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc -> getMessage(), ""); 
}