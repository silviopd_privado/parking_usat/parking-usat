<?php

require_once '../datos/Conexion.clase.php';

class Registro extends Conexion {

    private $id_registro;
    private $dni;
    private $id_cargo;
    private $id_zona;
    private $nro_placa;
    private $id_tipo_vehiculo;
    private $id_marca;
    private $id_modelo;
    private $id_motivo;
    private $motivo;
    private $motivo_descripcion;
    private $dni_usuario;
    private $dni_acompanante;

    function getId_registro() {
        return $this->id_registro;
    }

    function getDni() {
        return $this->dni;
    }

    function getId_cargo() {
        return $this->id_cargo;
    }

    function getId_zona() {
        return $this->id_zona;
    }

    function getNro_placa() {
        return $this->nro_placa;
    }

    function getId_tipo_vehiculo() {
        return $this->id_tipo_vehiculo;
    }

    function getId_marca() {
        return $this->id_marca;
    }

    function getId_modelo() {
        return $this->id_modelo;
    }

    function getId_motivo() {
        return $this->id_motivo;
    }

    function getMotivo() {
        return $this->motivo;
    }

    function getMotivo_descripcion() {
        return $this->motivo_descripcion;
    }

    function getDni_usuario() {
        return $this->dni_usuario;
    }

    function getDni_acompanante() {
        return $this->dni_acompanante;
    }

    function setId_registro($id_registro) {
        $this->id_registro = $id_registro;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setId_cargo($id_cargo) {
        $this->id_cargo = $id_cargo;
    }

    function setId_zona($id_zona) {
        $this->id_zona = $id_zona;
    }

    function setNro_placa($nro_placa) {
        $this->nro_placa = $nro_placa;
    }

    function setId_tipo_vehiculo($id_tipo_vehiculo) {
        $this->id_tipo_vehiculo = $id_tipo_vehiculo;
    }

    function setId_marca($id_marca) {
        $this->id_marca = $id_marca;
    }

    function setId_modelo($id_modelo) {
        $this->id_modelo = $id_modelo;
    }

    function setId_motivo($id_motivo) {
        $this->id_motivo = $id_motivo;
    }

    function setMotivo($motivo) {
        $this->motivo = $motivo;
    }

    function setMotivo_descripcion($motivo_descripcion) {
        $this->motivo_descripcion = $motivo_descripcion;
    }

    function setDni_usuario($dni_usuario) {
        $this->dni_usuario = $dni_usuario;
    }

    function setDni_acompanante($dni_acompanante) {
        $this->dni_acompanante = $dni_acompanante;
    }

    public function listarRegistrar() {
        try {
            $sql = "select * from v_listar_registro";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarRegistrarDetalle() {
        try {
            $sql = "select * from f_listar_registro_acompanante(:p_id_registro)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_registro", $this->getId_registro());
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function agregarRegistro() {
        $this->dblink->beginTransaction();

        try {

            $sql = "select * from f_registrar(
                :p_dni,
                :p_id_cargo,
                :p_id_zona,
                :p_nro_placa,
                :p_id_tipo_vehiculo,
                :p_id_marca,
                :p_id_modelo,
                :p_id_motivo,
                :p_motivo,
                :p_motivo_descripcion,
                :p_dni_usuario,
                :p_dni_acompanante
        )";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->bindParam(":p_id_cargo", $this->getId_cargo());
            $sentencia->bindParam(":p_id_zona", $this->getId_zona());
            $sentencia->bindParam(":p_nro_placa", $this->getNro_placa());
            $sentencia->bindParam(":p_id_tipo_vehiculo", $this->getId_tipo_vehiculo());
            $sentencia->bindParam(":p_id_marca", $this->getId_marca());
            $sentencia->bindParam(":p_id_modelo", $this->getId_modelo());
            $sentencia->bindParam(":p_id_motivo", $this->getId_motivo());
            $sentencia->bindParam(":p_motivo", $this->getMotivo());
            $sentencia->bindParam(":p_motivo_descripcion", $this->getMotivo_descripcion());
            $sentencia->bindParam(":p_dni_usuario", $this->getDni_usuario());
            $sentencia->bindParam(":p_dni_acompanante", $this->getDni_acompanante());
            $sentencia->execute();

            $this->dblink->commit();

            return $sentencia->fetchAll(PDO::FETCH_ASSOC);
            //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function registroDarAlta() {
        try {
            $sql = "select * from f_dar_alta_registro(:p_id_registro);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_registro", $this->getId_registro());
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
