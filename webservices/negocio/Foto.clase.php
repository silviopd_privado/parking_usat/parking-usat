<?php

require_once '../datos/Conexion.clase.php';
require_once '../util/funciones/Funciones.clase.php';

class Foto extends Conexion
{

    /*
    public function foto_persona($dni)
    {
    $foto = "../imagenes/persona/" . $dni;

    if (file_exists($foto)) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS. $dni;
    } else {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . "no-image.png";
    }
    return $ruta;
    }

    public function foto_dni($dni)
    {
    $foto = "../imagenes/persona/" . $dni;

    if (glob($foto . ".*")) {
    if (file_exists($foto . ".JPG")) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . $dni . ".JPG";
    } else if (file_exists($foto . ".jpg")) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . $dni . ".jpg";
    } else if (file_exists($foto . ".PNG")) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . $dni . ".PNG";
    } else if (file_exists($foto . ".png")) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . $dni . ".png";
    } else if (file_exists($foto . ".JPEG")) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . $dni . ".JPEG";
    } else if (file_exists($foto . ".jpeg")) {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . $dni . ".jpeg";
    }
    } else {
    $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_PERSONAS . "no-image.png";
    }

    return $ruta;
    }
     */

    public function foto_usuario($dni)
    {
        $foto = "../imagenes/administradores/" . $dni . "A";

        if (glob($foto . ".*")) {
            if (file_exists($foto . ".JPG")) {
                $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . $dni . "A" . ".JPG";
            } else if (file_exists($foto . ".jpg")) {
                $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . $dni . "A" . ".jpg";
            } else if (file_exists($foto . ".PNG")) {
                $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . $dni . "A" . ".PNG";
            } else if (file_exists($foto . ".png")) {
                $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . $dni . "A" . ".png";
            } else if (file_exists($foto . ".JPEG")) {
                $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . $dni . "A" . ".JPEG";
            } else if (file_exists($foto . ".jpeg")) {
                $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . $dni . "A" . ".jpeg";
            }
        } else {
            $ruta = Funciones::$DIRECCION_WEB_SERVICE_FOTO_ADMINISTRADORES . "no-image.png";
        }

        return $ruta;
    }

}
