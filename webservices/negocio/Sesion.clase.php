<?php

require_once '../datos/Conexion.clase.php';

class Sesion extends Conexion{
        
    public function iniciarSesionWeb($dni,$clave) {
    	try{
        $sql = "select * from f_validar_sesion_admin(:p_dni,:p_clave)";
        $sentencia = $this->dblink->prepare($sql);
        
	$sentencia->bindParam(":p_dni", $dni);
	$sentencia->bindParam(":p_clave", $clave);
	$sentencia->execute();
	$resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

        if($sentencia->rowCount()){
            return $resultado;
        }else{
          throw new Exception("Correo o contraseña incorrectos.");
        }
    	}catch(PDOException $ex){
    		throw new \Exception($ex->getMessage());
    	}
    }  
 
        
}

