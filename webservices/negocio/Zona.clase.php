<?php

require_once '../datos/Conexion.clase.php';

class Zona extends Conexion {
    

    public function listarRegistrar(){
        try {
            $sql = "select * from zona where id_zona!='001' order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
}
