<?php

require_once '../datos/Conexion.clase.php';
require_once '../util/funciones/Funciones.clase.php';

class Persona extends Conexion {

    private $dni;
    private $codigo_universitario;
    private $apellido_paterno;
    private $apellido_materno;
    private $nombres;
    private $telefono;
    private $correo;
    private $estado;
    private $id_cargo;
    private $id_zona;

    public function buscarPersona() {
        $this->dblink->beginTransaction();

        try {

            $sql = "select * from f_buscar_persona(:p_dni)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

            $this->dblink->commit();

            return $resultado;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

    public function buscarPersona2() {
        $this->dblink->beginTransaction();

        try {

            $sql = "SELECT 
                    persona.dni, 
                    persona.codigo_universitario, 
                    persona.apellido_paterno, 
                    persona.apellido_materno, 
                    persona.nombres, 
                    persona.telefono, 
                    persona.correo, 
                    persona.estado, 
                    persona.id_cargo, 
                    persona.id_zona, 
                    fotos_persona.ruta as ruta_a,
                    (select fotos_persona.ruta from fotos_persona where fotos_persona.dni=:p_dni order by 1 desc limit 1 ) as ruta_b
                  FROM 
                    public.persona, 
                    public.fotos_persona
                  WHERE 
                    persona.dni = fotos_persona.dni and persona.dni = :p_dni
                  LIMIT 1;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

            $this->dblink->commit();

            return $resultado;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

    public function listarPersona() {
        try {
            $sql = "select * from v_listar_persona";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarPersonaAutocomplete() {
        try {
            $sql = "select * from v_listar_persona_autocomplete";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function buscarPersonaImagen() {
        $this->dblink->beginTransaction();

        try {

            $sql = "select * from f_buscar_persona_imagen(:p_dni)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            $this->dblink->commit();

            return $resultado;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

    function getDni() {
        return $this->dni;
    }

    function getCodigo_universitario() {
        return $this->codigo_universitario;
    }

    function getApellido_paterno() {
        return $this->apellido_paterno;
    }

    function getApellido_materno() {
        return $this->apellido_materno;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getEstado() {
        return $this->estado;
    }

    function getId_cargo() {
        return $this->id_cargo;
    }

    function getId_zona() {
        return $this->id_zona;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setCodigo_universitario($codigo_universitario) {
        $this->codigo_universitario = $codigo_universitario;
    }

    function setApellido_paterno($apellido_paterno) {
        $this->apellido_paterno = $apellido_paterno;
    }

    function setApellido_materno($apellido_materno) {
        $this->apellido_materno = $apellido_materno;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setId_cargo($id_cargo) {
        $this->id_cargo = $id_cargo;
    }

    function setId_zona($id_zona) {
        $this->id_zona = $id_zona;
    }

}
