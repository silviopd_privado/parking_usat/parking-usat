<?php

require_once '../datos/Conexion.clase.php';

class Marca extends Conexion {

    public function listarRegistrar($p_id_tipo_vehiculo) {
        try {
            $sql = "SELECT 
                    marca.id_marca, 
                    marca.nombre, 
                    marca.id_tipo_vehiculo
                  FROM 
                    public.tipo_vehiculo, 
                    public.marca
                  WHERE 
                    marca.id_tipo_vehiculo = tipo_vehiculo.id_tipo_vehiculo
                    AND marca.id_tipo_vehiculo = :p_id_tipo_vehiculo 
                  ORDER BY 2;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_tipo_vehiculo", $p_id_tipo_vehiculo);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
