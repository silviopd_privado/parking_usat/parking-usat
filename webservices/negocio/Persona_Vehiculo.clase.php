<?php

require_once '../datos/Conexion.clase.php';

class Persona_Vehiculo extends Conexion {
    
    
    private $dni;
    private $codigo_universitario;
    private $apellido_paterno;
    private $apellido_materno;
    private $nombres;
    private $telefono;
    private $correo;
    private $id_cargo;
    private $id_zona;
    private $nro_placa;
    private $id_tipo_vehiculo;
    private $id_marca;
    private $id_modelo;
    private $color;
    private $foto;
    private $fotodnia;
    private $fotodnib;
    private $fotocarroa;
    private $fotocarrob;
    private $fotocarroc;
    private $fotocarrod;

    function getDni() {
        return $this->dni;
    }

    function getCodigo_universitario() {
        return $this->codigo_universitario;
    }

    function getApellido_paterno() {
        return $this->apellido_paterno;
    }

    function getApellido_materno() {
        return $this->apellido_materno;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getId_cargo() {
        return $this->id_cargo;
    }

    function getId_zona() {
        return $this->id_zona;
    }

    function getNro_placa() {
        return $this->nro_placa;
    }

    function getId_tipo_vehiculo() {
        return $this->id_tipo_vehiculo;
    }

    function getId_marca() {
        return $this->id_marca;
    }

    function getId_modelo() {
        return $this->id_modelo;
    }

    function getColor() {
        return $this->color;
    }

    function getFotodnia() {
        return $this->fotodnia;
    }

    function getFotodnib() {
        return $this->fotodnib;
    }

    function getFotocarroa() {
        return $this->fotocarroa;
    }

    function getFotocarrob() {
        return $this->fotocarrob;
    }

    function getFotocarroc() {
        return $this->fotocarroc;
    }

    function getFotocarrod() {
        return $this->fotocarrod;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setCodigo_universitario($codigo_universitario) {
        $this->codigo_universitario = $codigo_universitario;
    }

    function setApellido_paterno($apellido_paterno) {
        $this->apellido_paterno = $apellido_paterno;
    }

    function setApellido_materno($apellido_materno) {
        $this->apellido_materno = $apellido_materno;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setId_cargo($id_cargo) {
        $this->id_cargo = $id_cargo;
    }

    function setId_zona($id_zona) {
        $this->id_zona = $id_zona;
    }

    function setNro_placa($nro_placa) {
        $this->nro_placa = $nro_placa;
    }

    function setId_tipo_vehiculo($id_tipo_vehiculo) {
        $this->id_tipo_vehiculo = $id_tipo_vehiculo;
    }

    function setId_marca($id_marca) {
        $this->id_marca = $id_marca;
    }

    function setId_modelo($id_modelo) {
        $this->id_modelo = $id_modelo;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setFotodnia($fotodnia) {
        $this->fotodnia = $fotodnia;
    }

    function setFotodnib($fotodnib) {
        $this->fotodnib = $fotodnib;
    }

    function setFotocarroa($fotocarroa) {
        $this->fotocarroa = $fotocarroa;
    }

    function setFotocarrob($fotocarrob) {
        $this->fotocarrob = $fotocarrob;
    }

    function setFotocarroc($fotocarroc) {
        $this->fotocarroc = $fotocarroc;
    }

    function setFotocarrod($fotocarrod) {
        $this->fotocarrod = $fotocarrod;
    }
    
    function getFoto() {
        return $this->foto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    
    public function agregarRegistrar() {
        $this->dblink->beginTransaction();

        try {

            $sql = "select * from f_persona_vehiculo_registrar(
                :p_dni,
                :p_codigo_universitario,
                :p_apellido_paterno,
                :p_apellido_materno,
                :p_nombres,
                :p_telefono,
                :p_correo,
                :p_id_cargo,
                :p_id_zona,
                :p_nro_placa,
                :p_id_tipo_vehiculo,
                :p_id_marca,
                :p_id_modelo,
                :p_color,
                :p_foto,
                :p_fotodnia,
                :p_fotodnib,
                :p_fotocarroa,
                :p_fotocarrob,
                :p_fotocarroc,
                :p_fotocarrod
            )";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni", $this->getDni());
            $sentencia->bindParam(":p_codigo_universitario", $this->getCodigo_universitario());
            $sentencia->bindParam(":p_apellido_paterno", $this->getApellido_paterno());
            $sentencia->bindParam(":p_apellido_materno", $this->getApellido_materno());
            $sentencia->bindParam(":p_nombres", $this->getNombres());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_correo", $this->getCorreo());
            $sentencia->bindParam(":p_id_cargo", $this->getId_cargo());
            $sentencia->bindParam(":p_id_zona", $this->getId_zona());
            $sentencia->bindParam(":p_nro_placa", $this->getNro_placa());
            $sentencia->bindParam(":p_id_tipo_vehiculo", $this->getId_tipo_vehiculo());
            $sentencia->bindParam(":p_id_marca", $this->getId_marca());
            $sentencia->bindParam(":p_id_modelo", $this->getId_modelo());
            $sentencia->bindParam(":p_color", $this->getColor());
            $sentencia->bindParam(":p_foto", $this->getFoto());
            $sentencia->bindParam(":p_fotodnia", $this->getFotodnia());
            $sentencia->bindParam(":p_fotodnib", $this->getFotodnib());
            $sentencia->bindParam(":p_fotocarroa", $this->getFotocarroa());
            $sentencia->bindParam(":p_fotocarrob", $this->getFotocarrob());
            $sentencia->bindParam(":p_fotocarroc", $this->getFotocarroc());
            $sentencia->bindParam(":p_fotocarrod", $this->getFotocarrod());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }
    
    public function updatePersonaRegistrar2($dni,$ruta) {
        $this->dblink->beginTransaction();

        try {

            $sql = "UPDATE public.persona SET foto=:p_ruta WHERE dni=:p_dni;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_ruta", $ruta);
            $sentencia->bindParam(":p_dni", $dni);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }
    

    public function updatePersonaRegistrar($dni,$lado,$ruta) {
        $this->dblink->beginTransaction();

        try {

            $sql = "UPDATE public.fotos_persona SET ruta=:p_ruta WHERE dni=:p_dni and lado=:p_lado";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_ruta", $ruta);
            $sentencia->bindParam(":p_dni", $dni);
            $sentencia->bindParam(":p_lado", $lado);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function updateVehiculoRegistrar($nro_placa,$lado,$ruta) {
        $this->dblink->beginTransaction();

        try {

            $sql = "UPDATE public.fotos_vehiculo SET ruta=:p_ruta WHERE nro_placa=:p_nro_placa and lado=:p_lado";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_ruta", $ruta);
            $sentencia->bindParam(":p_nro_placa", $nro_placa);
            $sentencia->bindParam(":p_lado", $lado);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

}