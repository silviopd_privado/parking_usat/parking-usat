<?php

require_once '../datos/Conexion.clase.php';

class Tipo_Vehiculo extends Conexion {
    

    public function listarRegistrar(){
        try {
            $sql = "select * from tipo_vehiculo order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
}
