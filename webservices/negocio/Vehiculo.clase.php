<?php

require_once '../datos/Conexion.clase.php';

class Vehiculo extends Conexion {

    private $nro_placa;
    private $color;
    private $estado;
    private $id_tipo_vehiculo;
    private $id_marca;
    private $id_modelo;

    public function buscarVehiculo() {
        $this->dblink->beginTransaction();

        try {

            $sql = "select * from f_buscar_vehiculo(:p_nro_placa)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nro_placa", $this->getNro_placa());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

            $this->dblink->commit();

            return $resultado;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

    public function listarVehiculo() {
        try {
            $sql = "select * from v_listar_vehiculo";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    function getNro_placa() {
        return $this->nro_placa;
    }

    function getColor() {
        return $this->color;
    }

    function getEstado() {
        return $this->estado;
    }

    function getId_tipo_vehiculo() {
        return $this->id_tipo_vehiculo;
    }

    function getId_marca() {
        return $this->id_marca;
    }

    function getId_modelo() {
        return $this->id_modelo;
    }

    function setNro_placa($nro_placa) {
        $this->nro_placa = $nro_placa;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setId_tipo_vehiculo($id_tipo_vehiculo) {
        $this->id_tipo_vehiculo = $id_tipo_vehiculo;
    }

    function setId_marca($id_marca) {
        $this->id_marca = $id_marca;
    }

    function setId_modelo($id_modelo) {
        $this->id_modelo = $id_modelo;
    }

}
