<?php

require_once '../datos/Conexion.clase.php';

class Cargo extends Conexion {
    

    public function listarRegistrar(){
        try {
            $sql = "select * from cargo where id_cargo!=5 order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
}
