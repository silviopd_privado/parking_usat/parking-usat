<?php

require_once '../datos/Conexion.clase.php';

class Modelo extends Conexion {

    public function listarRegistrar($p_id_tipo_vehiculo,$p_id_marca) {
        try {
            $sql = "SELECT 
                    modelo.id_modelo, 
                    modelo.nombre, 
                    modelo.id_marca, 
                    modelo.id_tipo_vehiculo
                  FROM 
                    public.tipo_vehiculo, 
                    public.marca, 
                    public.modelo
                  WHERE 
                    marca.id_tipo_vehiculo = tipo_vehiculo.id_tipo_vehiculo AND
                    marca.id_marca = modelo.id_marca AND
                    marca.id_tipo_vehiculo = modelo.id_tipo_vehiculo 
                    AND modelo.id_tipo_vehiculo=:p_id_tipo_vehiculo AND modelo.id_marca=:p_id_marca
                  ORDER BY 2;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_tipo_vehiculo", $p_id_tipo_vehiculo);
            $sentencia->bindParam(":p_id_marca", $p_id_marca);            
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
