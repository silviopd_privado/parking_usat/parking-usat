package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class Persona {
    private String dni;
    private String nombre_completo;
    private String zona;

    public static ArrayList<Zona> listaSerie = new ArrayList<>();

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre_completo() {
        return nombre_completo;
    }

    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }
}
