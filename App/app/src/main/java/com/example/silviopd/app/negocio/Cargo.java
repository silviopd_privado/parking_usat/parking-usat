package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class Cargo {

    private int id_cargo;
    private String nombre;

    public static ArrayList<Cargo> listaSerie = new ArrayList<>();

    public int getId_cargo() {
        return id_cargo;
    }

    public void setId_cargo(int id_cargo) {
        this.id_cargo = id_cargo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
