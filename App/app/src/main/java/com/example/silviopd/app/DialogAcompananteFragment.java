package com.example.silviopd.app;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.silviopd.app.negocio.Zona;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONObject;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogAcompananteFragment extends DialogFragment {

    public  int res;
    private EditText txtDniAcompanante;
    Button btncancelardialog,btnaceptardialog;
    //TCAcompanante tareaAcompanante;
    int cargo_acompanante;
    String zona_acompanante;

    public DialogAcompananteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_acompanante, container, false);

        txtDniAcompanante = (EditText) view.findViewById(R.id.txtdniacompanante);
        btnaceptardialog = (Button) view.findViewById(R.id.btnaceptardialog);
        btncancelardialog = (Button) view.findViewById(R.id.btncancelardialog);

        txtDniAcompanante.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


        btncancelardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        btnaceptardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TCAcompanante().execute(txtDniAcompanante.getText().toString());
            }
        });

        return view;
    }

    // Call this method to send the data back to the parent fragment
    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        DialogAcompanante listener = (DialogAcompanante) getTargetFragment();
        Log.e("ACOMPANANTEEEEE1",String.valueOf(res));
        listener.DataDialogAcompanante(res,txtDniAcompanante.getText().toString());
        dismiss();
    }

    public interface DialogAcompanante{
        void DataDialogAcompanante(int res,String dni);
    }

    public class TCAcompanante extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            String dni = (String) params[0];

            String ws = Funciones.URL_WS+ "persona.buscar.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("dni",String.valueOf(dni));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        // TODO: ARREGLAR CODIGO PARA LLAMAR DESDE DIALOG A OTRO DIALOG
        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONObject jsonArray = json.getJSONObject("datos");

                        Log.i("RESULTADOOOO1",String.valueOf(jsonArray.getInt("resultado")));
                        Log.i("RESULTADOOOO2",String.valueOf(res));
                        res = jsonArray.getInt("resultado");
                        Log.i("RESULTADOOOO3",String.valueOf(res));
                        cargarDatos(jsonArray.getInt("resultado"));
                        sendBackResult();
                    }
                }catch (Exception e){
                    Log.e("ERROR",e.getMessage());
                }
            }
        }
    }

    public void cargarDatos(int resultado){
        res=resultado;
        Log.i("RESULTADOOOO4",String.valueOf(res));
    }

}
