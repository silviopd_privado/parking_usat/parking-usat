package com.example.silviopd.app;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Marca;
import com.example.silviopd.app.negocio.Modelo;
import com.example.silviopd.app.negocio.TipoVehiculo;
import com.example.silviopd.app.util.Funciones;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogRegistrarFragment extends DialogFragment {

    Button btncancelardialog,btnaceptardialog;
    TextView txtdni,txtnroplaca;
    ImageView imagen;
    int resultado;

    public DialogRegistrarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_registrar, container, false);

        getDialog().setTitle("¿REGISTRAR?");
        resultado = 0;

        btnaceptardialog = (Button) view.findViewById(R.id.btnaceptardialog);
        btncancelardialog = (Button) view.findViewById(R.id.btncancelardialog);

        txtdni = (TextView) view.findViewById(R.id.dialog_registrar_dni);
        txtnroplaca = (TextView) view.findViewById(R.id.dialog_registrar_nro_placa);
        imagen = (ImageView) view.findViewById(R.id.dialog_registrar_imagen);

        Bundle mArgs = getArguments();
        String dni = mArgs.getString("dni");

        txtdni.setText(dni);
        txtnroplaca.setText(mArgs.getString("nro_placa"));

        // Image url
        //String image_url = "http://www.usat.edu.pe/web/wp-content/uploads/2015/07/logousat.png";

        //CargarImagen tarea1 = new CargarImagen();
        //tarea1.execute( image_url );

        new TCBuscarPersona().execute(dni);

        btncancelardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultado = 0;
                getDialog().dismiss();
            }
        });

        btnaceptardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultado = 1;
                sendBackResult();
            }
        });

        return view;
    }


    private Bitmap descargarImagen (String direccion){
        URL imageUrl = null;
        Bitmap imagen = null;
        try{
            imageUrl = new URL(direccion);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            imagen = BitmapFactory.decodeStream(conn.getInputStream());
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return imagen;
    }

    private class CargarImagen extends AsyncTask<String, Void, Bitmap> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Descargando imágen");
            pDialog.setProgressStyle( ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String imagenDescargar = params[0];

            Bitmap imagenDescargada = descargarImagen(imagenDescargar);

            return imagenDescargada;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imagen.setImageBitmap( result );
            pDialog.dismiss();
        }
    }

    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        DialogRegistrar listener = (DialogRegistrar) getTargetFragment();
        listener.DataDialogRegistrar(resultado);
        dismiss();
    }

    public interface DialogRegistrar{
        void DataDialogRegistrar(int resultado);
    }

    public class TCBuscarPersona extends AsyncTask<Object, Void, String> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("BUSCANDO...");
            pDialog.setProgressStyle( ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Object... params) {
            String dni = (String) params[0];

            String ws = Funciones.URL_WS+ "persona.buscar.imagen.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("dni",String.valueOf(dni));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        //JSONObject jsonArray = json.getJSONObject("datos");
                        JSONArray jsonArray = json.getJSONArray("datos");
                        Log.e("ARRAY", String.valueOf(jsonArray));

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        int res= jsonObject.getInt("resultado");
                        Log.i("RESULTADOOOOOOO",String.valueOf(res));
                        String foto = jsonObject.getString("foto");
                        Log.i("FOTOOOOOOOOO",String.valueOf(foto));

                        if (res==1){
                            CargarImagen tarea1 = new CargarImagen();
                            tarea1.execute(Funciones.URL_WS+"../imagenes/personas/"+foto);
                        }
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            pDialog.dismiss();
        }
    }
}
