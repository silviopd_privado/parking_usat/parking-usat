package com.example.silviopd.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.silviopd.app.negocio.Registro;

import java.util.ArrayList;

public class AdaptadorRegistroPersona extends BaseAdapter {

    public static ArrayList<Registro> listaRegistro;
    private LayoutInflater layoutInflater;

    public static ArrayList<Registro> getListaRegistro() {
        return listaRegistro;
    }

    public void setListaRegistro( ArrayList<Registro> listaRegistro) {
        AdaptadorRegistroPersona.listaRegistro = listaRegistro;
        notifyDataSetChanged();
    }

    public AdaptadorRegistroPersona(Context con, ArrayList<Registro> cli) {
        this.layoutInflater=LayoutInflater.from(con);
        this.listaRegistro=cli;
    }

    @Override
    public int getCount() {
        return listaRegistro.size();
    }

    @Override
    public Object getItem(int position) {
        return listaRegistro.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final holder h;
        if (convertView==null){
            convertView=layoutInflater.inflate(R.layout.item_daralta_persona,null);
            h=new holder();
            h.txtfecha =(TextView) convertView.findViewById(R.id.p_fec2);
            h.txthora =(TextView) convertView.findViewById(R.id.p_fec3);
            h.bloque_seccion =(TextView) convertView.findViewById(R.id.p_blo2);
            h.bloque_numero =(TextView) convertView.findViewById(R.id.p_blo3);
            h.dni =(TextView) convertView.findViewById(R.id.p_dni2);
            h.placa =(TextView) convertView.findViewById(R.id.p_pla2);
            h.nombre =(TextView) convertView.findViewById(R.id.p_nom2);
            h.visitante =(TextView) convertView.findViewById(R.id.p_vi2);
            h.motivo_nombre =(TextView) convertView.findViewById(R.id.p_mot2);
            h.motivo_descripcion =(TextView) convertView.findViewById(R.id.p_des2);
            convertView.setTag(h);
        }else{
            h=(holder)convertView.getTag();
        }

        h.txtfecha.setText(listaRegistro.get(position).getFecha_entrada());
        h.txthora.setText(listaRegistro.get(position).getHora_entrada());
        h.bloque_seccion.setText(listaRegistro.get(position).getBloque());
        h.bloque_numero.setText(String.valueOf(listaRegistro.get(position).getNumero()));
        h.dni.setText(listaRegistro.get(position).getDni());
        h.placa.setText(listaRegistro.get(position).getNro_placa());
        h.nombre.setText(listaRegistro.get(position).getNombres());
        h.visitante.setText(listaRegistro.get(position).getId_visitante());
        h.motivo_nombre.setText(listaRegistro.get(position).getNombre_motivo());
        h.motivo_descripcion.setText(listaRegistro.get(position).getMotivo_descripcion());

        return convertView;
    }

    static class holder{
        TextView txtfecha;
        TextView txthora;
        TextView bloque_seccion;
        TextView bloque_numero;
        TextView dni;
        TextView placa;
        TextView nombre;
        TextView visitante;
        TextView motivo_nombre;
        TextView motivo_descripcion;
    }
}
