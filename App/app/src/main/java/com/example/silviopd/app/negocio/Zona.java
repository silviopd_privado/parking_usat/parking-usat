package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class Zona {
    private String id_zona;
    private String nombre;
    public static int res;

    public static ArrayList<Zona> listaSerie = new ArrayList<>();

    public String getId_zona() {
        return id_zona;
    }

    public void setId_zona(String id_zona) {
        this.id_zona = id_zona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
