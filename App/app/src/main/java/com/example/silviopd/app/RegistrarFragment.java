package com.example.silviopd.app;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Acompanante;
import com.example.silviopd.app.negocio.Marca;
import com.example.silviopd.app.negocio.Persona;
import com.example.silviopd.app.negocio.Zona;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrarFragment extends Fragment implements DialogAcompananteFragment.DialogAcompanante,DialogPersonaFragment.DialogPersona,DialogVehiculoFragment.DialogVehiculo,DialogRegistrarFragment.DialogRegistrar{

    Button btnagregar_registrar;
    ListView lv_listado;

    AdaptadorAcompanante adaptador;
    ArrayList<Acompanante> listaClientes;

    Button btnbuscardni,btnbuscarnroplaca;

    EditText txtDni,txtNroPlaca;
    TCBuscarPersona tareaBuscarPersona;
    TCBuscarVehiculo tareaBuscarVehiculo;
    private int cargo;
    private String zona,dni;
    public int persona_acompanante;

    TextView txtcargopersona,txtzonapersona,txttipovehiculo,txtmarca,txtmodelo;

    CheckBox chk_zona,chk_profesor;
    AutoCompleteTextView txtMotivoNombre;

    ArrayList<Zona> arrayListZona=new ArrayList<Zona>();
    ArrayList<String> NomZona=new ArrayList<String>();
    ArrayAdapter objArrayAdapter;
    ArrayList<Persona> arrayListPersona=new ArrayList<Persona>();

    Button registrar;
    EditText txtmotivo_descripcion;
    static String motivo;

    Button btnbarcode;

    public RegistrarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_registrar, container, false);

        btnagregar_registrar = (Button) view.findViewById(R.id.btnagregar_registrar);
        lv_listado = (ListView) view.findViewById(R.id.lv_acompanantes);

        txtcargopersona = (TextView) view.findViewById(R.id.txtcargo_registrar);
        txtzonapersona = (TextView) view.findViewById(R.id.txtzona_registrar);

        txttipovehiculo = (TextView) view.findViewById(R.id.txttipovehiculo_registrar);
        txtmarca = (TextView) view.findViewById(R.id.txtmarca_registrar);
        txtmodelo = (TextView) view.findViewById(R.id.txtmodelo_registrar);

        listaClientes = new ArrayList<Acompanante>();
        adaptador = new AdaptadorAcompanante(getContext(), listaClientes);
        lv_listado.setAdapter(adaptador);

        chk_zona = (CheckBox) view.findViewById(R.id.re_rbbut);
        chk_profesor = (CheckBox) view.findViewById(R.id.re_rbbut2);
        txtMotivoNombre = (AutoCompleteTextView) view.findViewById(R.id.re_text3);

        objArrayAdapter=new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,NomZona);
        txtMotivoNombre.setAdapter(objArrayAdapter);

        registrar = (Button) view.findViewById(R.id.re_btn);
        txtmotivo_descripcion = (EditText) view.findViewById(R.id.re_des) ;

        btnbarcode = (Button) view.findViewById(R.id.btnbarcode);

        txtMotivoNombre.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (chk_zona.isChecked()){
                    for (int j = 0; j < arrayListZona.size(); j++) {
                        if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(arrayListZona.get(j).getNombre())){
                            Toast.makeText(getContext(),arrayListZona.get(j).getId_zona(),Toast.LENGTH_LONG).show();
                            motivo =  arrayListZona.get(j).getId_zona();
                        }
                    }
                }

                if (chk_profesor.isChecked()){
                    for (int j = 0; j < arrayListPersona.size(); j++) {
                        if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(arrayListPersona.get(j).getNombre_completo())){
                            Toast.makeText(getContext(),arrayListPersona.get(j).getZona(),Toast.LENGTH_LONG).show();
                            motivo =  arrayListPersona.get(j).getZona();
                        }
                    }
                }
            }
        });

        btnagregar_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAcompananteFragment dialog = new DialogAcompananteFragment();
                dialog.setTargetFragment(RegistrarFragment.this, 1);
                dialog.show(getFragmentManager(), "MyCustomDialog");
            }
        });

        btnbuscardni = (Button) view.findViewById(R.id.btnbuscardni);
        btnbuscarnroplaca = (Button) view.findViewById(R.id.btnbuscarnroplaca);

        txtDni = (EditText) view.findViewById(R.id.re_text);
        txtNroPlaca = (EditText) view.findViewById(R.id.re_text2);

        btnbuscardni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dni = txtDni.getText().toString();
                new TCBuscarPersona().execute(dni);
            }
        });

        btnbuscarnroplaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nro_placa = txtNroPlaca.getText().toString();
                new TCBuscarVehiculo().execute(nro_placa);
            }
        });

       chk_zona.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               new TCZona().execute();

               txtMotivoNombre.setText("");
               txtMotivoNombre.setEnabled(true);
               txtMotivoNombre.setFocusableInTouchMode(true);

               if (chk_profesor.isChecked()){
                   chk_profesor.setChecked(false);
               }
           }
       });

        chk_profesor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TCPersona().execute();

                txtMotivoNombre.setText("");
                txtMotivoNombre.setEnabled(true);
                txtMotivoNombre.setFocusableInTouchMode(true);

                if (chk_zona.isChecked()){
                    chk_zona.setChecked(false);
                }
            }
        });

        chk_zona.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!chk_zona.isChecked() && !chk_profesor.isChecked()){
                    txtMotivoNombre.setText("");
                    txtMotivoNombre.setEnabled(false);
                    txtMotivoNombre.setFocusableInTouchMode(false);
                    txtMotivoNombre.setFocusable(false);
                }
            }
        });

        chk_profesor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!chk_zona.isChecked() && !chk_profesor.isChecked()){
                    txtMotivoNombre.setText("");
                    txtMotivoNombre.setEnabled(false);
                    txtMotivoNombre.setFocusable(false);
                }
            }
        });

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle args = new Bundle();
                args.putString("dni", txtDni.getText().toString());
                args.putString("nro_placa", txtNroPlaca.getText().toString());

                DialogRegistrarFragment dialog = new DialogRegistrarFragment();
                dialog.setTargetFragment(RegistrarFragment.this, 1);
                dialog.setArguments(args);
                dialog.show(getFragmentManager(), "DialogRegistrarFragment");

                /*
                boolean r = Funciones.mensajeConfirmacion(getContext(), "Confirme", "Desea registrar...");
                if(r) {
                    String dni = txtDni.getText().toString();
                    int id_cargo = Integer.parseInt(txtcargopersona.getText().toString());
                    String id_zona = txtzonapersona.getText().toString();
                    String nro_placa = txtMroPlaca.getText().toString();
                    int id_tipo_vehiculo = Integer.parseInt(txttipovehiculo.getText().toString());
                    int id_marca = Integer.parseInt(txtmarca.getText().toString());
                    int id_modelo = Integer.parseInt(txtmodelo.getText().toString());

                    int id_motivo;
                    if (chk_zona.isChecked() || chk_profesor.isChecked()) {
                        id_motivo = 2;
                    } else {
                        id_motivo = 1;
                    }

                    String motivo = RegistrarFragment.motivo;
                    String motivo_descripcion = txtmotivo_descripcion.getText().toString();
                    String dni_usuario = "46705014";

                    //Generando el detalle de la venta en JSON
                    JSONArray jsonArrayDet = new JSONArray();
                    for (int i = 0; i< AdaptadorAcompanante.listaDatos.size(); i++){
                        jsonArrayDet.put(AdaptadorAcompanante.listaDatos.get(i).getJSONObject());
                    }
                    String p_dni_acompanante= jsonArrayDet.toString();
                    Log.e("Detalle Acomp", p_dni_acompanante);

                    new GrabarRegistroTask(dni,id_cargo,id_zona,nro_placa,id_tipo_vehiculo,id_marca,id_modelo,id_motivo,motivo,motivo_descripcion,dni_usuario,p_dni_acompanante).execute();
                }
                */
            }
        });

        btnbarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity = new Intent(getActivity(), LecturaBarcodeActivity.class);
                startActivity(activity);
            }
        });

        return view;
    }

    @Override
    public void DataDialogRegistrar(int resultado) {
        if (resultado==1){
            String dni = txtDni.getText().toString();
            int id_cargo = Integer.parseInt(txtcargopersona.getText().toString());
            String id_zona = txtzonapersona.getText().toString();
            String nro_placa = txtNroPlaca.getText().toString();
            int id_tipo_vehiculo = Integer.parseInt(txttipovehiculo.getText().toString());
            int id_marca = Integer.parseInt(txtmarca.getText().toString());
            int id_modelo = Integer.parseInt(txtmodelo.getText().toString());

            int id_motivo;
            if (chk_zona.isChecked() || chk_profesor.isChecked()) {
                id_motivo = 2;
            } else {
                id_motivo = 1;
            }

            String motivo = RegistrarFragment.motivo;
            String motivo_descripcion = txtmotivo_descripcion.getText().toString();
            String dni_usuario = "46705014";

            //Generando el detalle de la venta en JSON
            JSONArray jsonArrayDet = new JSONArray();
            for (int i = 0; i< AdaptadorAcompanante.listaDatos.size(); i++){
                jsonArrayDet.put(AdaptadorAcompanante.listaDatos.get(i).getJSONObject());
            }
            String p_dni_acompanante= jsonArrayDet.toString();
            Log.e("Detalle Acomp", p_dni_acompanante);

            new GrabarRegistroTask(dni,id_cargo,id_zona,nro_placa,id_tipo_vehiculo,id_marca,id_modelo,id_motivo,motivo,motivo_descripcion,dni_usuario,p_dni_acompanante).execute();
        }
    }

    public class GrabarRegistroTask extends AsyncTask<Void, Void, String> {
        private String dni;
        private int id_cargo;
        private  String id_zona;
        private  String nro_placa;
        private  int id_tipo_vehiculo;
        private  int id_marca;
        private  int id_modelo;
        private  int id_motivo;
        private String motivo;
        private  String motivo_descripcion;
        private String dni_usuario;
        private String p_dni_acompanante;

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("GUARDANDO....");
            pDialog.setProgressStyle( ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        public GrabarRegistroTask(String dni, int id_cargo, String id_zona, String nro_placa, int id_tipo_vehiculo, int id_marca, int id_modelo, int id_motivo, String motivo, String motivo_descripcion, String dni_usuario, String p_dni_acompanante) {
            this.dni = dni;
            this.id_cargo = id_cargo;
            this.id_zona = id_zona;
            this.nro_placa = nro_placa;
            this.id_tipo_vehiculo = id_tipo_vehiculo;
            this.id_marca = id_marca;
            this.id_modelo = id_modelo;
            this.id_motivo = id_motivo;
            this.motivo = motivo;
            this.motivo_descripcion = motivo_descripcion;
            this.dni_usuario = dni_usuario;
            this.p_dni_acompanante = p_dni_acompanante;
        }

        @Override
        protected String doInBackground(Void... params) {
            // LLamar a la WS para grabar la venta
            try {
                String ws = Funciones.URL_WS + "registro.agregar.php";
                HashMap parametros = new HashMap<String, String>();

                parametros.put("dni",String.valueOf(this.dni));
                parametros.put("id_cargo",String.valueOf(this.id_cargo));
                parametros.put("id_zona",String.valueOf(this.id_zona));
                parametros.put("nro_placa",String.valueOf(this.nro_placa));
                parametros.put("id_tipo_vehiculo",String.valueOf(this.id_tipo_vehiculo));
                parametros.put("id_marca",String.valueOf(this.id_marca));
                parametros.put("id_modelo",String.valueOf(this.id_modelo));
                parametros.put("id_motivo",String.valueOf(this.id_motivo));
                parametros.put("motivo",String.valueOf(this.motivo));
                parametros.put("motivo_descripcion",String.valueOf(this.motivo_descripcion));
                parametros.put("dni_usuario",String.valueOf(this.dni_usuario));
                parametros.put("dni_acompanante",String.valueOf(this.p_dni_acompanante));

                String resultado = new Funciones().getHttpContent(ws, parametros);
                return resultado;

            } catch (Exception e) {
                System.out.println(e.getMessage());
                return "";
            }
        }


        protected void onPostExecute(final String resultado) {
            Log.e("resultado",resultado);
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);

                    int estado = json.getInt("estado");
                    String mensaje = json.getString("mensaje");

                    if (estado==200) {
                        if (mensaje.equalsIgnoreCase("REGISTRO SATISFACTORIO")){
                            JSONArray datos = json.getJSONArray("datos");
                            JSONObject dato = datos.getJSONObject(0);

                            String bloque = dato.getString("bloque");
                            int numero = dato.getInt("numero");

                            Funciones.mensajeInformacion(getContext(),"Registrado Correctamente","IR A BLOQUE "+bloque+"-"+numero);
                        }else{
                            Funciones.mensajeInformacion(getContext(),"Información",mensaje);
                        }

                        txtDni.setText("");
                        txtcargopersona.setText("1");
                        txtzonapersona.setText("1");
                        txtNroPlaca.setText("");
                        txttipovehiculo.setText("1");
                        txtmarca.setText("1");
                        txtmodelo.setText("1");
                        txtMotivoNombre.setText("");
                        txtmotivo_descripcion.setText("");
                        this.motivo = "";
                        AdaptadorAcompanante.listaDatos.clear();
                    }else{
                        Funciones.mensajeError(getContext(),"Error",json.getString("mensaje"));
                    }
                }catch (Exception e){
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            pDialog.dismiss();
        }

    }



    @Override
    public void DataDialogPersona(int cargo, String zona) {
        if (persona_acompanante==0){
            Log.e("TP/MA/MO",String.valueOf(cargo)+" - "+String.valueOf(zona));
            txtcargopersona.setText(String.valueOf(cargo));
            txtzonapersona.setText(zona);
        }else{
            Acompanante obj=new Acompanante();
            obj.setTxtDni(dni);
            obj.setTxtCargo(cargo);
            obj.setTxtZona(zona);
            listaClientes.add(obj);
            adaptador.setListaDatos(listaClientes);
        }
        persona_acompanante = 0;
    }

    @Override
    public void DataDialogVehiculo(int tipo_vehiculo, int marca, int modelo) {
        Log.e("TP/MA/MO",String.valueOf(tipo_vehiculo)+" - "+String.valueOf(marca)+" - "+String.valueOf(modelo));
        txttipovehiculo.setText(String.valueOf(tipo_vehiculo));
        txtmarca.setText(String.valueOf(marca));
        txtmodelo.setText(String.valueOf(modelo));
    }

    @Override
    public void DataDialogAcompanante(int res, String dni) {

        this.dni = dni;
        if (res ==1){
            persona_acompanante = 1;
            DialogPersonaFragment dialog = new DialogPersonaFragment();
            dialog.setTargetFragment(RegistrarFragment.this, 1);
            dialog.show(getFragmentManager(), "DialogPersonaFragment");
        }else{
            Acompanante obj=new Acompanante();
            obj.setTxtDni(dni);
            obj.setTxtCargo(res);
            obj.setTxtZona("");
            listaClientes.add(obj);
            adaptador.setListaDatos(listaClientes);
        }
    }


    public class TCBuscarPersona extends AsyncTask<Object, Void, String> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("BUSCANDO...");
            pDialog.setProgressStyle( ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Object... params) {
            String dni = (String) params[0];

            String ws = Funciones.URL_WS+ "persona.buscar.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("dni",String.valueOf(dni));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONObject jsonArray = json.getJSONObject("datos");

                        int res= jsonArray.getInt("resultado");
                        Log.e("DFSAFASDF",String.valueOf(res));

                        if (res==1){
                            DialogPersonaFragment dialog = new DialogPersonaFragment();
                            dialog.setTargetFragment(RegistrarFragment.this, 1);
                            dialog.show(getFragmentManager(), "DialogPersonaFragment");
                        }
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            pDialog.dismiss();
        }
    }

    public class TCBuscarVehiculo extends AsyncTask<Object, Void, String> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("BUSCANDO...");
            pDialog.setProgressStyle( ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Object... params) {
            String nro_placa = (String) params[0];

            String ws = Funciones.URL_WS+ "vehiculo.buscar.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("nro_placa",String.valueOf(nro_placa));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONObject jsonArray = json.getJSONObject("datos");

                        int res= jsonArray.getInt("resultado");
                        //Log.i("RESULTAAAAAAADO",String.valueOf(res));

                        if (res==1){
                            DialogVehiculoFragment dialog = new DialogVehiculoFragment();
                            dialog.setTargetFragment(RegistrarFragment.this, 1);
                            dialog.show(getFragmentManager(), "DialogVehiculoFragment");
                        }
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            pDialog.dismiss();
        }
    }

    public class TCZona extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            String ws = Funciones.URL_WS+ "zona.listar.reg.php";
            HashMap parametros = new HashMap<String,String>();
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        NomZona.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Zona objSerie = new Zona();
                            objSerie.setId_zona(objItem.getString("id_zona"));
                            objSerie.setNombre(objItem.getString("nombre"));
                            arrayListZona.add(objSerie);
                            NomZona.add(objItem.getString("nombre"));
                        }
                    }

                    objArrayAdapter=new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,NomZona);
                    txtMotivoNombre.setAdapter(objArrayAdapter);
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCPersona extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            String ws = Funciones.URL_WS+ "persona.listar.autocomplete.php";
            HashMap parametros = new HashMap<String,String>();
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        // TODO: ARREGLAR CODIGO PARA LLAMAR DESDE DIALOG A OTRO DIALOG
        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        NomZona.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Persona objSerie = new Persona();
                            objSerie.setDni(objItem.getString("dni"));
                            objSerie.setNombre_completo(objItem.getString("nombre_completo"));
                            objSerie.setZona(objItem.getString("id_zona"));
                            arrayListPersona.add(objSerie);
                            NomZona.add(objItem.getString("nombre_completo"));
                        }
                    }

                    objArrayAdapter=new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,NomZona);
                    txtMotivoNombre.setAdapter(objArrayAdapter);
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
