package com.example.silviopd.app;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class InicialActivity extends AppCompatActivity {

    Animation frombottom,fromtop;
    View logo;
    TextView parking,usat;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicial);

        logo = (View) findViewById(R.id.in_logo);
        parking = (TextView) findViewById(R.id.in_parking);
        usat = (TextView) findViewById(R.id.in_usat);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        parking.setAnimation(frombottom);
        usat.setAnimation(frombottom);
        logo.setAnimation(fromtop);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent activity = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(activity);
            }
        }, 3500);
    }
}
