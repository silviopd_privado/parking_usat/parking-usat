package com.example.silviopd.app;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Acompanante;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AdaptadorAcompanante extends BaseAdapter{

    public static ArrayList<Acompanante> listaDatos;
    private LayoutInflater layoutInflater;

    public static ArrayList<Acompanante> getListaDatos() {
        return listaDatos;
    }

    public void setListaDatos(ArrayList<Acompanante> listaDatos) {
        AdaptadorAcompanante.listaDatos = listaDatos;
        notifyDataSetChanged();
    }

    public AdaptadorAcompanante(Context con, ArrayList<Acompanante> cli) {
        this.layoutInflater=LayoutInflater.from(con);
        this.listaDatos=cli;
    }

    @Override
    public int getCount() {
        return listaDatos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaDatos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final holder h;
        if (convertView==null){
            convertView=layoutInflater.inflate(R.layout.item_acompanantes,null);
            h=new holder();
            h.txtdni_item =(EditText) convertView.findViewById(R.id.txtdni_item);
            h.btneliminar_item = (Button) convertView.findViewById(R.id.btneliminar_item);
            h.txtcargo_item = (TextView) convertView.findViewById(R.id.txtcargo_item);
            h.txtzona_item = (TextView) convertView.findViewById(R.id.txtzona_item);
            convertView.setTag(h);
        }else{
            h=(holder)convertView.getTag();
        }
        h.txtdni_item.setText(listaDatos.get(position).getTxtDni());
        h.txtzona_item.setText(listaDatos.get(position).getTxtZona());
        h.txtcargo_item.setText(String.valueOf(listaDatos.get(position).getTxtCargo()));

        h.btneliminar_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaDatos.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    static class holder{
        EditText txtdni_item;
        Button btneliminar_item;
        TextView txtcargo_item;
        TextView txtzona_item;
    }



}
