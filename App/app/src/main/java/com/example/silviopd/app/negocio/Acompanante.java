package com.example.silviopd.app.negocio;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Acompanante {

    private String txtDni;
    private int txtCargo;
    private String txtZona;

    public String getTxtDni() {
        return txtDni;
    }

    public void setTxtDni(String txtDni) {
        this.txtDni = txtDni;
    }

    public int getTxtCargo() {
        return txtCargo;
    }

    public void setTxtCargo(int txtCargo) {
        this.txtCargo = txtCargo;
    }

    public String getTxtZona() {
        return txtZona;
    }

    public void setTxtZona(String txtZona) {
        this.txtZona = txtZona;
    }

    public JSONObject getJSONObject(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("dni",this.getTxtDni());
            obj.put("id_cargo",this.getTxtCargo());
            obj.put("id_zona",this.getTxtZona());

        }catch(JSONException e) {
            Log.e("error", e.getMessage());
        }
        return obj;
    }
}
