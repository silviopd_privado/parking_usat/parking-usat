package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class RegistroDetalle {

    private int id_registro;
    private int nro_detalle;
    private String dni_detalle;
    private String nombre;

    public static ArrayList<RegistroDetalle> listaRegistroDetalle = new ArrayList<>();

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public int getNro_detalle() {
        return nro_detalle;
    }

    public void setNro_detalle(int nro_detalle) {
        this.nro_detalle = nro_detalle;
    }

    public String getDni_detalle() {
        return dni_detalle;
    }

    public void setDni_detalle(String dni_detalle) {
        this.dni_detalle = dni_detalle;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
