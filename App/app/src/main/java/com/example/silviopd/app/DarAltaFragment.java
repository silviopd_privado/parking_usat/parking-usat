package com.example.silviopd.app;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Registro;
import com.example.silviopd.app.negocio.RegistroDetalle;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DarAltaFragment extends Fragment {

    ListView lvPersona,lvAcompanante;

    AdaptadorRegistroPersona adaptador;
    ArrayList<Registro> listaRegistro;

    AdaptadorRegistroDetalle adaptador2;
    ArrayList<RegistroDetalle> listaRegistroDetalle;

    public DarAltaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dar_alta, container, false);

        lvPersona = (ListView) view.findViewById(R.id.lvPersona);
        lvAcompanante = (ListView) view.findViewById(R.id.lvAcompanante);

        listaRegistro = new ArrayList<Registro>();
        adaptador = new AdaptadorRegistroPersona(getContext(), listaRegistro);
        lvPersona.setAdapter(adaptador);

        listaRegistroDetalle = new ArrayList<RegistroDetalle>();
        adaptador2 = new AdaptadorRegistroDetalle(getContext(), listaRegistroDetalle);
        lvAcompanante.setAdapter(adaptador2);

        new TCRegistro().execute();

        lvPersona.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {               ;
                new TCRegistroDetalle().execute( listaRegistro.get(i).getId_registro());
            }
        });

        lvPersona.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                boolean r = Funciones.mensajeConfirmacion(getContext(), "Confirme", "Desea dar de alta a nro placa: "+listaRegistro.get(i).getNro_placa());
                if(r) {
                    new TCDarAlta().execute(listaRegistro.get(i).getId_registro());
                }
                return false;
            }
        });


        return view;
    }

    public class TCRegistro extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            String ws = Funciones.URL_WS+ "registro.listar.php";
            HashMap parametros = new HashMap<String,String>();
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        listaRegistro.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Registro objSerie = new Registro();

                            objSerie.setId_registro(objItem.getInt("id_registro"));
                            objSerie.setFecha_entrada(objItem.getString("fecha_entrada"));
                            objSerie.setHora_entrada(objItem.getString("hora_entrada"));
                            objSerie.setMotivo(objItem.getString("motivo"));
                            objSerie.setNombre_motivo(objItem.getString("nombre_motivo"));
                            objSerie.setBloque(objItem.getString("bloque"));
                            objSerie.setNumero(objItem.getInt("numero"));
                            objSerie.setNro_placa(objItem.getString("nro_placa"));
                            objSerie.setDni(objItem.getString("dni"));
                            objSerie.setEstado(objItem.getString("estado"));
                            objSerie.setMotivo_descripcion(objItem.getString("motivo_descripcion"));
                            objSerie.setCodigo_visitante(objItem.getString("codigo_visitante"));
                            objSerie.setNombres(objItem.getString("nombres"));
                            objSerie.setId_visitante(objItem.getString("id_visitante"));
                            listaRegistro.add(objSerie);
                        }
                        adaptador.setListaRegistro(listaRegistro);
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCRegistroDetalle extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            int id_registro = (Integer) params[0];

            String ws = Funciones.URL_WS+ "registro.detalle.listar.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("id_registro",String.valueOf(id_registro));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        listaRegistroDetalle.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            RegistroDetalle objSerie = new RegistroDetalle();

                            objSerie.setId_registro(objItem.getInt("id_registro"));
                            objSerie.setNro_detalle(objItem.getInt("nro_acompanante"));
                            objSerie.setDni_detalle(objItem.getString("dni_acompanante"));
                            objSerie.setNombre(objItem.getString("nombre_completo"));
                            listaRegistroDetalle.add(objSerie);
                        }
                        adaptador2.setListaRegistro(listaRegistroDetalle);
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCDarAlta extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            int id_registro = (int) params[0];

            String ws = Funciones.URL_WS+ "registro.dar.alta.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("id_registro",String.valueOf(id_registro));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        Toast.makeText(getActivity(), "SALIDA SATISFACTORIA", Toast.LENGTH_SHORT).show();
                        new TCRegistro().execute();
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
