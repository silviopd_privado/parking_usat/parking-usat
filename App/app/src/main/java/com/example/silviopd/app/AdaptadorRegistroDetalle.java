package com.example.silviopd.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.silviopd.app.negocio.RegistroDetalle;

import java.util.ArrayList;

public class AdaptadorRegistroDetalle extends BaseAdapter {

    public static ArrayList<RegistroDetalle> listaRegistroDetalle;
    private LayoutInflater layoutInflater;

    public static ArrayList<RegistroDetalle> getListaRegistroDetalle() {
        return listaRegistroDetalle;
    }

    public void setListaRegistro( ArrayList<RegistroDetalle> listaRegistroDetalle) {
        AdaptadorRegistroDetalle.listaRegistroDetalle = listaRegistroDetalle;
        notifyDataSetChanged();
    }

    public AdaptadorRegistroDetalle(Context con, ArrayList<RegistroDetalle> cli) {
        this.layoutInflater=LayoutInflater.from(con);
        this.listaRegistroDetalle=cli;
    }

    @Override
    public int getCount() {
        return listaRegistroDetalle.size();
    }

    @Override
    public Object getItem(int position) {
        return listaRegistroDetalle.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final holder h;
        if (convertView==null){
            convertView=layoutInflater.inflate(R.layout.item_daralta_acompanante,null);
            h=new holder();
            h.txtNro_detalle =(TextView) convertView.findViewById(R.id.a_n2);
            h.txtDni_detalle =(TextView) convertView.findViewById(R.id.a_dni2);
            h.txtNombre_detalle =(TextView) convertView.findViewById(R.id.a_nom2);
            h.checkbox = (CheckBox) convertView.findViewById(R.id.checkBox);

            convertView.setTag(h);
        }else{
            h=(holder)convertView.getTag();
        }

        h.txtNro_detalle.setText(String.valueOf(listaRegistroDetalle.get(position).getNro_detalle()));
        h.txtDni_detalle.setText(listaRegistroDetalle.get(position).getDni_detalle());
        h.txtNombre_detalle.setText(listaRegistroDetalle.get(position).getNombre());

        return convertView;
    }

    static class holder{
        CheckBox checkbox;
        TextView txtNro_detalle;
        TextView txtDni_detalle;
        TextView txtNombre_detalle;
    }
}
