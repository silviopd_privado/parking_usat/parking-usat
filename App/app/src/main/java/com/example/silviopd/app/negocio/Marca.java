package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class Marca {

    private int id_tipo_vehiculo;
    private int id_marca;
    private String nombre;

    public static ArrayList<Marca> listaSerie = new ArrayList<>();

    public int getId_tipo_vehiculo() {
        return id_tipo_vehiculo;
    }

    public void setId_tipo_vehiculo(int id_tipo_vehiculo) {
        this.id_tipo_vehiculo = id_tipo_vehiculo;
    }

    public int getId_marca() {
        return id_marca;
    }

    public void setId_marca(int id_marca) {
        this.id_marca = id_marca;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
