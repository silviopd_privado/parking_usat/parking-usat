package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class Registro {
    private int id_registro;
    private String fecha_entrada;
    private String hora_entrada;
    private String motivo;
    private String nombre_motivo;
    private String bloque;
    private int numero;
    private String nro_placa;
    private String dni;
    private String estado;
    private String motivo_descripcion;
    private String codigo_visitante;
    private String nombres;
    private String id_visitante;
    //private String id_motivo;
    //private String dni_usuario;
    //private String fecha_salida;
    //private String hora_salida;

    public static ArrayList<Registro> listaRegistro = new ArrayList<>();

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public String getFecha_entrada() {
        return fecha_entrada;
    }

    public void setFecha_entrada(String fecha_entrada) {
        this.fecha_entrada = fecha_entrada;
    }

    public String getHora_entrada() {
        return hora_entrada;
    }

    public void setHora_entrada(String hora_entrada) {
        this.hora_entrada = hora_entrada;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getNombre_motivo() {
        return nombre_motivo;
    }

    public void setNombre_motivo(String nombre_motivo) {
        this.nombre_motivo = nombre_motivo;
    }

    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNro_placa() {
        return nro_placa;
    }

    public void setNro_placa(String nro_placa) {
        this.nro_placa = nro_placa;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMotivo_descripcion() {
        return motivo_descripcion;
    }

    public void setMotivo_descripcion(String motivo_descripcion) {
        this.motivo_descripcion = motivo_descripcion;
    }

    public String getCodigo_visitante() {
        return codigo_visitante;
    }

    public void setCodigo_visitante(String codigo_visitante) {
        this.codigo_visitante = codigo_visitante;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getId_visitante() {
        return id_visitante;
    }

    public void setId_visitante(String id_visitante) {
        this.id_visitante = id_visitante;
    }
}
