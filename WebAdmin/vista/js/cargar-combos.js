function cargarComboTipoVehiculo(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "tipo_vehiculo.listar.reg.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un tipo de vehiculo</option>';
            } else {
                html += '<option value="0">Todas los tipos de vehiculos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_tipo_vehiculo + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboMarca(p_nombreCombo, p_tipo, p_id_tipo_vehiculo) {
    var ruta = DIRECCION_WS + "marca.listar.reg.php";

    $.post(ruta, {id_tipo_vehiculo: p_id_tipo_vehiculo}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una marca</option>';
            } else {
                html += '<option value="0">Todas las marcas</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_marca + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboModelo(p_nombreCombo, p_tipo, p_id_tipo_vehiculo, p_id_marca) {
    var ruta = DIRECCION_WS + "modelo.listar.reg.php";

    $.post(ruta, {id_tipo_vehiculo: p_id_tipo_vehiculo, id_marca: p_id_marca}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un modelo</option>';
            } else {
                html += '<option value="0">Todos los modelos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_modelo + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboCargo(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "cargo.listar.reg.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un cargo</option>';
            } else {
                html += '<option value="0">Todos los cargos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_cargo + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboZona(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "zona.listar.reg.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una zona</option>';
            } else {
                html += '<option value="0">Todos los cargos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_zona + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}