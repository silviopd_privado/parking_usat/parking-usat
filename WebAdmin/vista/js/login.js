$("#frminiciosesion").submit(function (evento) {
    evento.preventDefault();

    var ruta = DIRECCION_WS + "sesion.validar.web.php";
    var txtDni = $("#login-username").val();
    var txtPassword = md5($("#login-password").val());

    $.post(ruta, {dni: txtDni, clave: txtPassword}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        
        if (datosJSON.estado === 200) {
            Cookies.set('token', datosJSON.datos.token);
            Cookies.set('foto', datosJSON.datos.foto);
            Cookies.set('nombre_completo', datosJSON.datos.nombre_completo);
            Cookies.set('cargo', datosJSON.datos.cargo);
            Cookies.set('telefono', datosJSON.datos.telefono);
            Cookies.set('correo', datosJSON.datos.correo);

            window.location = "./home.html";
        } else {
            $("#login-username").val("");
            $("#login-password").val("");
            $("#login-username").focus();
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        $("#login-username").val("");
        $("#login-password").val("");
        $("#login-username").focus();

        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    });
});