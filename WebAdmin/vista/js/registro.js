$(document).ready(function() {

    $("#imagen-menu").attr("src", Cookies.get("foto"));
    $("#nombre-menu").text(Cookies.get("nombre_completo"));
    $("#nombre-header").text(Cookies.get("nombre_completo"));

    listar();
});

function listar() {

    var ruta = DIRECCION_WS + "registro.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-bordered table-striped nowrap" id="tabla-listado">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-center"></th>';
            html += '<th class="text-center">Dni</th>';
            html += '<th class="text-center">Nombre</th>';
            html += '<th class="text-center">Placa</th>';
            html += '<th class="text-center">Bloque</th>';
            html += '<th class="text-center">Fecha</th>';
            html += '<th class="text-center">Hora</th>';
            html += '<th class="text-center">Visitante</th>';
            html += '<th class="text-center">Cod. Vi.</th>';
            html += '<th class="text-center">Motivo</th>';            
            html += '<th class="text-center">Nombre</th>';
            html += '<th class="text-center">Descripcion</th>';
            html += '<th class="text-center">Opciones</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td class="text-center">'+item.id_registro+'</td>';
                html += '<td class="text-center">'+item.dni+'</td>';
                html += '<td class="text-center">'+item.nombres+'</td>';
                html += '<td class="text-center">'+item.nro_placa+'</td>';
                html += '<td class="text-center">'+item.bloque+' - '+item.numero+'</td>';
                html += '<td class="text-center">'+item.fecha_entrada+'</td>';
                html += '<td class="text-center">'+item.hora_entrada+'</td>';
                html += '<td class="text-center">'+item.id_visitante+'</td>';
                html += '<td class="text-center">'+item.codigo_visitante+'</td>';
                html += '<td class="text-center">'+item.motivo+'</td>';
                html += '<td class="text-center">'+item.nombre_motivo+'</td>';
                html += '<td class="text-center">'+item.motivo_descripcion+'</td>';
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-pencil" style="color:#FF5A00;"></i></button><button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-times" style="color:#FF0000;"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#tabla-listado').dataTable({
                "aaSorting": [
                    [1, "asc"]
                ],
                "scrollX": true
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}