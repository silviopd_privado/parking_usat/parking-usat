$(document).ready(function() {

    $("#imagen-menu").attr("src", Cookies.get("foto"));
    $("#nombre-menu").text(Cookies.get("nombre_completo"));
    $("#nombre-header").text(Cookies.get("nombre_completo"));

    listar();

    cargarComboCargo("#cbocargo", "seleccione");

    cargarComboZona("#cbozona", "seleccione");
});

$("#cbocargo").change(function() {
    var id_cargo = $("#cbocargo").val();
    if (id_cargo == 1) {
        $("#cbozona").val("002");
        $("#cbozona").attr("disabled", true);
    } else {
        $("#cbozona").val("");
        $("#cbozona").attr("disabled", false);
        //deshabilitar una opcion
        //$("select option:contains('ESTUDIANTE')").attr("disabled","disabled");

        //esconder una opcion
        $("#cbozona option[value=002]").hide();
    }
});

function listar() {

    var ruta = DIRECCION_WS + "persona.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-bordered table-striped nowrap" id="tabla-listado">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-center">Dni</th>';
            html += '<th class="text-center">Cod. Uni.</th>';
            html += '<th class="text-center">Nombre</th>';
            html += '<th class="text-center">Telefono</th>';
            html += '<th class="text-center">Correo</th>';
            html += '<th class="text-center">Cargo</th>';
            html += '<th class="text-center">Zona</th>';
            html += '<th class="text-center">Estado</th>';
            html += '<th class="text-center">Opciones</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td class="text-center">' + item.dni + '</td>';
                html += '<td class="text-center">' + item.codigo_universitario + '</td>';
                html += '<td class="text-center">' + item.nombre_completo + '</td>';
                html += '<td class="text-center">' + item.telefono + '</td>';
                html += '<td class="text-center">' + item.correo + '</td>';
                html += '<td class="text-center">' + item.cargo_nombre + '</td>';
                html += '<td class="text-center">' + item.zona_nombre + '</td>';
                if (item.estado == 'H') {
                    html += '<td class="text-center">HABILITADO</td>';
                } else {
                    html += '<td class="text-center">DESHABILITADO</td>';
                }
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-popout" onclick="leerdatos(\'' + item.dni + '\')"><i class="fa fa-pencil" style="color:#FF5A00;"></i></button>';
                html += '<button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-times" style="color:#FF0000;"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#tabla-listado').dataTable({
                "aaSorting": [
                    [1, "asc"]
                ],
                "scrollX": true
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function leerdatos(dni) {
    var ruta = DIRECCION_WS + "persona.buscar2.php";

    $.post(ruta, { dni: dni }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var item = datosJSON.datos;
            $("#txttipooperacion").val("editar");
            $("#txttitulo").val("EDITAR");
            $("#txtdni").val(item.dni);
            $("#txtcodigouniversitario").val(item.codigo_universitario);
            $("#txtapellidopaterno").val(item.apellido_paterno);
            $("#txtapellidomaterno").val(item.apellido_materno);
            $("#txtnombrecompleto").val(item.nombres);
            $("#txttelefono").val(item.telefono);
            $("#txtcorreo").val(item.correo);
            $("#cbocargo").val(item.id_cargo);
            $("#cbozona").val(item.id_zona);
            $("#cboestado").val(item.estado);
            $("#fotodnia_imagen").attr("src", DIRECCION_WS_FOTO_PERSONAS + item.ruta_a);
            $("#fotodnib_imagen").attr("src", DIRECCION_WS_FOTO_PERSONAS + item.ruta_b);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$("#btnagregar").click(function() {
    $("#txttipooperacion").val("agregar");
    $("#txttitulo").val("AGREGAR");
    $("#txtdni").val("");
    $("#txtcodigouniversitario").val("");
    $("#txtapellidopaterno").val("");
    $("#txtapellidomaterno").val("");
    $("#txtnombrecompleto").val("");
    $("#txttelefono").val("");
    $("#txtcorreo").val("");
    $("#cbocargo").val("");
    $("#cbozona").val("");
    $("#cboestado").val("H");

    $("#fotodnia_imagen").removeAttr("src");
    $("#fotodnib_imagen").removeAttr("src");
    $("#fotodnia_imagen").attr("src", "imagenes/dnia.png");
    $("#fotodnib_imagen").attr("src", "imagenes/dnib.jpg");
});

$("#frmgrabar").submit(function(evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function(isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var token = $.cookie('token');
                var dni = $("#txtcodigo").val()
                var apellidopaterno = $("#txtapellidopaterno").val()
                var apellidomaterno = $("#txtapellidomaterno").val()
                var nombre = $("#txtnombre").val()
                var cargo = $("#cbocargomodal").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var correo = $("#txtcorreo").val()

                
            } else {
            }
        }
    });
});