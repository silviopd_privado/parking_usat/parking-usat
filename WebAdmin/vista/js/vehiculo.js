$(document).ready(function(){

    $("#imagen-menu").attr("src", Cookies.get("foto"));
    $("#nombre-menu").text(Cookies.get("nombre_completo"));
    $("#nombre-header").text(Cookies.get("nombre_completo"));

    listar();

    cargarComboTipoVehiculo("#cbotipovehiculo", "seleccione");
});

$("#cbotipovehiculo").change(function() {
  var id_tipo_vehiculo = $("#cbotipovehiculo").val();
  cargarComboMarca("#cbomarca", "seleccione", id_tipo_vehiculo);
});

$("#cbomarca").change(function() {
  var id_tipo_vehiculo = $("#cbotipovehiculo").val();
  var id_marca = $("#cbomarca").val();
  cargarComboModelo("#cbomodelo", "seleccione", id_tipo_vehiculo, id_marca);
});

function listar() {

    var ruta = DIRECCION_WS + "vehiculo.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-bordered table-striped table-vcenter js-dataTable-full" id="tabla-listado">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-center">Placa</th>';
            html += '<th class="d-none d-sm-table-cell">Tipo</th>';
            html += '<th class="d-none d-sm-table-cell">Marca</th>';
            html += '<th class="d-none d-sm-table-cell">Modelo</th>';
            html += '<th class="d-none d-sm-table-cell">Color</th>';
            html += '<th class="d-none d-sm-table-cell">Estado</th>';
            html += '<th class="text-center" style="width: 15%;">Profile</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td class="text-center">'+item.nro_placa+'</td>';
                html += '<td class="d-none d-sm-table-cell">'+item.tipo_vehiculo_nombre+'</td>';
                html += '<td class="d-none d-sm-table-cell">'+item.marca_nombre+'</td>';
                html += '<td class="d-none d-sm-table-cell">'+item.modelo_nombre+'</td>';
                html += '<td class="d-none d-sm-table-cell">'+item.color+'</td>';
                if (item.estado =='H') {
                    html += '<td class="d-none d-sm-table-cell">HABILITADO</td>';
                }else{
                    html += '<td class="d-none d-sm-table-cell">DESHABILITADO</td>';
                }
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-popout" onclick="leerdatos(\'' + item.nro_placa + '\')"><i class="fa fa-pencil" style="color:#FF5A00;"></i></button>';
                html += '<button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-times" style="color:#FF0000;"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#tabla-listado').dataTable({
                "aaSorting": [
                    [1, "asc"]
                ],
                scrollY: true
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function leerdatos(dni) {
    var ruta = DIRECCION_WS + "persona.buscar2.php";

    $.post(ruta, { dni: dni }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var item = datosJSON.datos;
            $("#txttitulo").val("EDITAR");
            $("#txtdni").val(item.dni);
            $("#txtcodigouniversitario").val(item.codigo_universitario);
            $("#txtapellidopaterno").val(item.apellido_paterno);
            $("#txtapellidomaterno").val(item.apellido_materno);
            $("#txtnombrecompleto").val(item.nombres);
            $("#txttelefono").val(item.telefono);
            $("#txtcorreo").val(item.correo);
            $("#cbocargo").val(item.id_cargo);
            $("#cbozona").val(item.id_zona);
            $("#cboestado").val(item.estado);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}