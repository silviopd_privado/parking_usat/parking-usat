$(document).ready(function() {

    $("#imagen-menu").attr("src", Cookies.get("foto"));
    $("#nombre-menu").text(Cookies.get("nombre_completo"));
    $("#nombre-header").text(Cookies.get("nombre_completo"));

    listar();
});

function listar() {

    var ruta = DIRECCION_WS + "registro.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-bordered table-striped table-vcenter js-dataTable-full" id="tabla-listado">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-center">N°</th>';
            html += '<th>Dni</th>';
            html += '<th class="d-none d-sm-table-cell">Nombre</th>';
            html += '<th class="d-none d-sm-table-cell">Placa</th>';
            html += '<th class="d-none d-sm-table-cell">Bloque</th>';

            html += '<th class="text-center" style="width: 15%;">Profile</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td class="text-center">' + item.id_registro + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.dni + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.nombres + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.nro_placa + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.bloque + ' - ' + item.numero + '</td>';
                if (item.id_registro == 1) {
                    html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary active" onclick="leerDatos('+item.id_registro+')" id="btnPersona'+item.id_registro+'"><i class="fa fa-check" style="color:#FF5A00;"></i></button>';
                }else{
                   html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary" onclick="leerDatos('+item.id_registro+')" id="btnPersona'+item.id_registro+'"><i class="fa fa-check" style="color:#FF5A00;"></i></button>';
                }
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#tabla-listado').dataTable({
                "aaSorting": [
                    [1, "asc"]
                ]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    });

    var ruta2 = DIRECCION_WS + "registro.detalle.listar.php";

    $.post(ruta2,{"id_registro":1}, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-bordered table-striped table-vcenter js-dataTable-full" id="tabla-listado2">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-center">N°</th>';
            html += '<th>Dni</th>';
            html += '<th class="d-none d-sm-table-cell">Nombre</th>';
            html += '<th class="text-center" style="width: 15%;">Profile</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td class="text-center">' + item.nro_acompanante + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.dni_acompanante + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.nombre_completo + '</td>';
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-check" style="color:#FF5A00;"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado2").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [
                    [1, "asc"]
                ]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function leerDatos(id_registro) {
    $("#btnPersona1").removeClass('active');
    $(this).addClass('active').siblings().removeClass('active');

    var ruta2 = DIRECCION_WS + "registro.detalle.listar.php";

    $.post(ruta2,{"id_registro":id_registro}, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-bordered table-striped table-vcenter js-dataTable-full" id="tabla-listado2">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="text-center">N°</th>';
            html += '<th>Dni</th>';
            html += '<th class="d-none d-sm-table-cell">Nombre</th>';
            html += '<th class="text-center" style="width: 15%;">Profile</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td class="text-center">' + item.nro_acompanante + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.dni_acompanante + '</td>';
                html += '<td class="d-none d-sm-table-cell">' + item.nombre_completo + '</td>';
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-check" style="color:#FF5A00;"></i></button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado2").html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}
